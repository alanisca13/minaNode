var downHandler = ('ontouchstart' in document.documentElement ? "touchstart" : "mousedown");
var upHandler = ('ontouchend' in document.documentElement ? "touchend" : "mouseup");
var moveHandler = ('ontouchmove' in document.documentElement ? "touchmove" : "mousemove");
document.addEventListener('DOMContentLoaded', function () {
    var caja = document.getElementById('target');
    var cursor = document.getElementById('normal-camera');
  
    var isDragging = false;
    var previousMouseX = 0;
    var rotationY = 0;
    cursor.addEventListener('mousedown', startDrag);
    cursor.addEventListener('touchstart', startDrag);

    document.addEventListener('mouseup', stopDrag);
    document.addEventListener('touchend', stopDrag);

    document.addEventListener('mousemove', drag);
    document.addEventListener('touchmove', drag);

    function startDrag(event) {
        isDragging = true;
        previousMouseX = getCursorX(event);
    }

    function stopDrag() {
        isDragging = false;
    }

    function drag(event) {
        if (isDragging && !vrMode) {
            var currentMouseX = getCursorX(event);
            var deltaX = currentMouseX - previousMouseX;
            var rotationFactor = 0.5;
            if(previousMouseX){
                var rotationY = caja.getAttribute('rotation').y + deltaX * rotationFactor;

                caja.setAttribute('rotation', { x: 0, y: rotationY, z: 0 });

                let isla_comuEL = document.querySelector('.isla_comuParent');
                let isla_medioEL = document.querySelector('.isla_medioParent');
                let isla_minaEL = document.querySelector('.isla_minaParent');
                let isla_procesEL = document.querySelector('.isla_procesParent');
                let isla_puertoEL = document.querySelector('.isla_puertoParent');
                isla_comuEL.setAttribute('rotation', { x: -25, y: -rotationY, z: 0 });
                isla_medioEL.setAttribute('rotation', { x: -25, y: -rotationY, z: 0 });
                isla_minaEL.setAttribute('rotation', { x: -25, y: -rotationY, z: 0 });
                isla_procesEL.setAttribute('rotation', { x: -25, y: -rotationY, z: 0 });
                isla_puertoEL.setAttribute('rotation', { x: -25, y: -rotationY, z: 0 });
                // Calcula la distancia recorrida por el mouse/touch
                var distanceTraveled = Math.abs(currentMouseX - previousMouseX);

                previousMouseX = currentMouseX;
            }
            else{
                previousMouseX = currentMouseX;
            }
        }
    }

    function getCursorX(event) {
        if (event.type.startsWith('touch')) {
            // Si es un evento táctil, utiliza la posición del primer dedo
            return event.touches[0].clientX;
        } else {
            // Si es un evento de clic, utiliza la posición del cursor del mouse
            return event.clientX;
        }
    }
});
// AFRAME.registerComponent('fcamera', {
//     /**
//      * We use IIFE (immediately-invoked function expression) to only allocate one
//      * vector or euler and not re-create on every tick to save memory.
//      */
//     tick: (function () {
//         var position = new THREE.Vector3();
//         var quaternion = new THREE.Quaternion();
    
//         return function () {
//             this.el.object3D.getWorldPosition(position);
//             this.el.object3D.getWorldQuaternion(quaternion);
//             // position and rotation now contain vector and quaternion in world space.
//         };
//     })()
// });