let SceneIsReady = false;
let vrMode = false;
let homeView = true;

let closeMenu = new Howl({src: [`assets/general/audio/closeMenu.mp3`]});
let fadeInFases = new Howl({src: [`assets/general/audio/fadeInFases.mp3`]});
let fadeOutFases = new Howl({src: [`assets/general/audio/fadeOutFases.mp3`]});
let fateTooggleFases = new Howl({src: [`assets/general/audio/fateTooggleFases.mp3`]});
let playButton = new Howl({src: [`assets/general/audio/playButton.mp3`]});
let playIsla = new Howl({src: [`assets/general/audio/playIsla.mp3`]});

let cursorVR = document.getElementById('cursorVR');
let normalCamera = document.getElementById('normal-camera');
let cameraContainer = document.getElementById('cameraContainer');

let generalScene;
let generalisland;

/* Regrear en escenas que no tienen ligas */
let clickIslan = "";
let clickScene = "";
let noPaces = 0;
document.querySelector('a-scene').addEventListener('enter-vr', function () {
    vrMode = true;
    cursorVR.setAttribute("visible","true");
    cursorVR.setAttribute("raycaster",{"far":"1000"});
    // cameraVR.setAttribute("camera","active",true);
    // cameraVR.setAttribute("visible",true);

    // document.querySelector('.isla_comuParent').setAttribute('rotation', `0 324 0`);
    // document.querySelector('.isla_medioParent').setAttribute('rotation', `0 36 0`);
    // document.querySelector('.isla_minaParent').setAttribute('rotation', `0 108 0`);
    // document.querySelector('.isla_procesParent').setAttribute('rotation', `0 252 0`);
    // document.querySelector('.isla_puertoParent').setAttribute('rotation', `0 180 0`);
    // normalCamera.removeAttribute("cursor")
    // normalCamera.setAttribute("cursor","rayOrigin","entity");
    // normalCamera.setAttribute("position","0 1.6 0");
    // normalCamera.setAttribute("look-controls",{"touchEnabled": "true", "mouseEnabled":"true", "magicWindowTrackingEnabled":"true"});
});
document.querySelector('a-scene').addEventListener('exit-vr', function () {
    vrMode = false;
    cursorVR.setAttribute("visible","false");
    cursorVR.setAttribute("raycaster",{"far":"1"});
    // cameraVR.setAttribute("visible","false");
    // normalCamera.removeAttribute("cursor")
    // normalCamera.setAttribute("cursor","rayOrigin","mouse");
    // cameraVR.setAttribute("camera","active",false);
    // cameraVR.setAttribute("visible",false);
    // normalCamera.setAttribute("camera","active",true);
    // normalCamera.setAttribute("visible",true);
    // normalCamera.setAttribute("camera","active",true);º  
    // normalCamera.setAttribute("camera","active",true);
    // normalCamera.setAttribute("look-controls","enabled","false");
    // normalCamera.removeAttribute('orbit-controls','enabled','true');
    // normalCamera.removeAttribute("cursor");
});

let videoPlaying;
let audioAmbiente = new Howl({src: [`assets/islas/procesamiento/procesamiento/audio/ambiente.mp3`],volume:.1, autoplay:false, loop: true});
let audioVoz = new Howl({src: [`assets/islas/procesamiento/centroControl/audio/voz.mp3`],volume:1, autoplay:false, loop: true});
AFRAME.registerComponent('btn-click', {
    init: function () {
        this.el.addEventListener('mouseenter', function (evt) {
            document.querySelectorAll('.boton').forEach((obj) => {
                obj.setAttribute('src','#'+obj.getAttribute("id-src"));
            });
            this.setAttribute('src','#'+this.getAttribute("id-src")+'_press');
        });
        this.el.addEventListener('mouseleave', function (evt) {
            this.setAttribute('src','#'+this.getAttribute("id-src"));
        });
        this.el.addEventListener('click', function (evt) {
            playButton.play();
            if(this.getAttribute('btn-click') == 'Liga'){
                SceneIsReady = false;
                /* STORAGE */
                handleClick(this.firstElementChild.getAttribute('id'));
                if(document.getElementById('sceneIsland').getAttribute("visible")) outIslands();
                clickLiga(this);
            }
            else{
                if( videoPlaying != document.getElementById(this.getAttribute('id-video')) ){
                    /* STORAGE */
                    handleClick(this.firstElementChild.getAttribute('id'));
                    if( videoPlaying != undefined && !videoPlaying.paused ){
                        videoPlaying.pause();
                        videoPlaying.currentTime = 0;
                    }
                }
                document.getElementById("persona").setAttribute("src","#"+this.getAttribute('id-video'));
                videoPlaying = document.getElementById(this.getAttribute('id-video'));
                videoPlaying.play();
            }
        });
    },
});

const customAssets = document.getElementById("custom-assets");
const customElements = document.getElementById("custom-elements");
const addNewAsset = (str) => {
    customAssets.insertAdjacentHTML('beforeend',str)
}
const addNewElement = (str) => {
    customElements.insertAdjacentHTML('beforeend',str)
}
const removeAssets = () => {
    while (customAssets.hasChildNodes()) {
        customAssets.removeChild(customAssets.firstChild);
    }
}
const removeElements = () => {
    while (customElements.hasChildNodes()) {
        customElements.removeChild(customElements.firstChild);
    }
}
const loadAssets = async (scene, island) => {
    for (let index = 0; index < eval(scene+"Array")[island].assets.length; index++) {
        addNewAsset(eval(scene+"Array")[island].assets[index]);
    }
}
const loadElements = async (scene, island) => {
    for (let index = 0; index < eval(scene+"Array")[island].elements.length; index++) {
        addNewElement(eval(scene+"Array")[island].elements[index]);
    }
    await Pace.restart();
    loopVideo = document.getElementsByClassName("loopVideo");
    for (let i = 0; i < loopVideo.length; i++) {
        loopVideo[i].play();
    }
    // readyScene();
}

const newScene = (scene, island) => {
    generalScene = scene;
    generalisland = island;
    removeElements();
    /*      Assets      */
    if(!document.getElementById(island+"-assets")) loadAssets(scene, island);
    /*      Elements    */
    loadElements(scene, island);
    /*      Camara      */
    // normalCamera.setAttribute('rotation',`0 ${eval(scene+"Array")[island].rotateCamera} 0`);
    // normalCamera.setAttribute('position',`0 ${eval(scene+"Array")[island].positionCamera} 0`);
}

// Pace.stop();
var caja = document.getElementById('sceneIsland');
Pace.on('hide', () => {
    if(noPaces > 0) {
        readyScene();
        regresarIslas.style.display = "block";
        anime({
            targets: caja,
            scale: [0, 1], // Escala desde 0 hasta 1
            rotate: '360deg', // Rotación de 360 grados
            easing: 'easeInOutQuad', // Tipo de interpolación
            duration: 2000, // Duración de la animación en milisegundos (ajusta según tus preferencias)
        });
    }
    else{
        upLoadPage();
    }
    noPaces++;
    SceneIsReady = true;
});

let backScene = "";
document.getElementById('regresar-atras').addEventListener("click",()=>{
    clickLiga(document.getElementById('regresar-atras'));
});

const readyScene = () => {
    /* Activar boton de regreso */
    if( eval(generalScene+"Array")[generalisland].onlyView ) {
        document.body.classList.add("regresar");
        // document.getElementById("regresar-atras").setAttribute('liga',`${generalScene}/${generalisland}`);
    }
    else{
        if(document.getElementById(generalisland+"_persona01")){
            document.getElementById(generalisland+"_persona01").play();
            document.getElementById(generalisland+"_persona01").pause();
            document.getElementById(generalisland+"_persona01").currentTime = 0;
        }
        // document.body.classList.remove("regresar");
        document.body.classList.add("regresar");
    }
    /* Retirar pantalla de carga */
    upLoadPage();
}
let goBack = "";
const clickLiga = (el) => {
    downLoadPage();
    loopVideo = document.getElementsByClassName("loopVideo");
    for (let i = 0; i < loopVideo.length; i++) {
        loopVideo[i].pause();
    }
    audioAmbiente.pause();
    audioVoz.pause();
    goBack = `${generalScene}/${generalisland}`;
    generalScene = el.getAttribute('liga').split('/')[0];
    generalisland = el.getAttribute('liga').split('/')[1];
    if(videoPlaying != undefined && !videoPlaying.paused){
        videoPlaying.pause();
        videoPlaying.currentTime = 0;
    }
    if(audioAmbiente != undefined && !audioAmbiente.paused){
        audioAmbiente.pause();
        audioAmbiente.currentTime = 0;
    }
    if(audioVoz != undefined && !audioVoz.paused){
        audioVoz.pause();
        audioVoz.currentTime = 0;
    }
    /* Play a audio de audioAmbiente */
    if( eval(generalScene+"Array")[generalisland].ambiente) {
        audioAmbiente = new Howl({src: [`assets/islas/${generalScene}/${generalisland}/audio/ambiente.mp3`],volume:.1, autoplay:false, loop: true});
    }
    /* Play a audio de audioVoz */
    if( eval(generalScene+"Array")[generalisland].voz) {
        audioVoz = new Howl({src: [`assets/islas/${generalScene}/${generalisland}/audio/voz.mp3`],volume:1, autoplay:false, loop: true});
    }
    setTimeout(() => {
        newScene(generalScene,generalisland);
        document.getElementById("regresar-atras").setAttribute('liga',goBack);
    }, 3000);
}


const outIslands = () => {
    setTimeout(() => {
        document.getElementById('sceneIsland').setAttribute("visible","false");
        document.getElementById('sceneIsland').setAttribute('scale','0 0 0');
        normalCamera.setAttribute("position","0 1.6 0");
        normalCamera.setAttribute("look-controls",{"touchEnabled": "true", "mouseEnabled":"true", "magicWindowTrackingEnabled":"true"});
    }, 2000);
}
const inIslands = () => {
    setTimeout(() => {
        removeElements();
        normalCamera.setAttribute("look-controls",{"touchEnabled": "false", "mouseEnabled":"false", "magicWindowTrackingEnabled":"false"});
        normalCamera.setAttribute("rotation","0 0 0")
        document.getElementById('sceneIsland').setAttribute("visible","true");
        document.getElementById('sceneIsland').setAttribute('scale','1 1 1');
        normalCamera.setAttribute("position","0 2 7");
    }, 2000);
}
const regresarIslas =document.getElementById('regresar-islas');
regresarIslas.style.display = "none";
regresarIslas.addEventListener("click", () => {
    location.reload();
    // audioAmbiente.pause();
    // audioVoz.pause();
    // downLoadPage();
    // removeElements();
    // inIslands();
    // setTimeout(() => {
    //     regresarIslas.style.display = "none";
    //     upLoadPage();
    // }, 1500);
});

let silenciar = () => {
    audioButton.classList.toggle('off');
    if( audioButton.classList.contains('off')) Howler.volume(0);
    else Howler.volume(.5)
}
const audioButton = document.getElementById('audioButton');
audioButton.addEventListener("click",silenciar);

$('.menu').on('click',function(){
    anime({
        targets: '.plecaContain',
        opacity: [0,1],
        easing: 'linear',
        duration: 500,
        begin: function(){
            playButton.play();
            $('.plecaContain').css('display','flex');
            anime({
                targets: '.contain.menus',
                scale: [0,1],
                easing: 'easeOutBack',
                duration: 1000,
                begin: function(){
                    $('.contain.menus').css('display','flex');
                }
            });
        },
    });
});
$('.plecaContain .menus .cerrar').on('click',function(){
    closeMenu.play();
    anime({
        targets: '.contain.menus',
        scale: [1,0],
        easing: 'easeInBack',
        duration: 1000,
        complete: function(){
            $('.contain.menus').css('display','none');
            anime({
                targets: '.plecaContain',
                opacity: [1,0],
                easing: 'linear',
                duration: 500,
                complete: function(){
                    $('.plecaContain').css('display','none');
                }
            });
        }
    });
});
let comoSeUsa = () =>{
    anime({
        targets: '.contain.menus',
        scale: [1,0],
        easing: 'easeInBack',
        duration: 1000,
        complete: function(){
            $('.contain.menus').css('display','none');
            anime({
                targets: '.contain.instrucciones',
                scale: [0,1],
                easing: 'linear',
                duration: 500,
                begin: function(){
                    $('.contain.instrucciones').css('display','flex');
                }
            });
        }
    });
    //Quitar Menú
    //Aparecer menú de como se usa
}
const restartPercent = () => {
    anime({
        targets: '.contain.menus',
        scale: [1,0],
        easing: 'easeInBack',
        duration: 1000,
        complete: function(){
            $('.contain.menus,.plecaContain').css('display','none');
            localStorage.removeItem('storedIDs');
            storedIDs = [];
            animateCounter( Math.trunc((storedIDs.length*100)/78) );
            $('a-image[src="#palomita"]').each(function(index){
                $(this).attr('scale','0 0 0');
            });
        }
    });
}
$('.plecaContain .menus .how').on('click', comoSeUsa);
$('.plecaContain .menus .reiniciar').on('click', restartPercent);