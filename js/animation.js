var home = anime.timeline({
    easing:'linear',
    autoplay: true, //Demos
    complete: () => {
        if(home.direction == 'normal'){
            document.getElementsByClassName('loadContain')[0].style.display = 'block';
            document.getElementsByClassName('loadContain')[0].style.backgroundColor = 'transparent';
        }
        if(home.direction == 'reverse'){
            document.getElementsByClassName('loadContain')[0].style.display = 'none';
            /* Play a audio de audioAmbiente */
            if(generalScene && generalisland){
                if( eval(generalScene+"Array")[generalisland].ambiente && !document.getElementById('sceneIsland').getAttribute("visible")) {
                    // audioAmbiente.src = `assets/islas/${generalScene}/${generalisland}/audio/Ambiente.mp3`;
                    audioAmbiente.play();
                }
                /* Play a audio de audioVoz */
                if( eval(generalScene+"Array")[generalisland].voz && !document.getElementById('sceneIsland').getAttribute("visible")) {
                    // audioVoz.src = `assets/islas/${generalScene}/${generalisland}/audio/voz.mp3`;
                    audioVoz.play();
                }
            }
        }
    },
    begin: () => {
        if(home.direction == 'normal'){
            document.getElementsByClassName('loadContain')[0].style.display = "block";
        }
    }
});
home.add({
    targets: '.loadContain .contain',
    top: ['-100%','0%'],
    delay: 500,
    duration:1000
})
.add({
    targets: '.pleca, .logo',
    opacity: 1,
    duration:1000
})
.add({
    targets: '.aros',
    easing: 'easeOutBack',
    scale: [0,1],
});
anime({
    targets: '.aros .aroBig',
    easing: 'linear',
    rotate: '360deg',
    duration: 8000,
    loop: true
});
anime({
    targets: '.aros .aroSmall',
    easing: 'linear',
    rotate: '-360deg',
    duration: 10000,
    loop: true
});

anime();

const downLoadPage = () => {
    home.direction = 'normal';
    home.play();
}
const upLoadPage = () => {
    if( home.paused ){
        setTimeout(() => {
            home.direction = 'reverse';
            home.play();
            /* STORAGE */
            animateElements();
        }, 1500);
    }
    else{
        setTimeout(() => {
            home.direction = 'reverse';
            home.play();
            /* STORAGE */
            animateElements();
        }, 3000);
    }

}