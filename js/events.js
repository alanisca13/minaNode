/* Clic  para comenzar */
document.getElementsByClassName('comenzar')[0].addEventListener("click",()=>{
    anime({
        targets:'.contain.introduccion',
        scale: [1,0],
        easing: 'easeInBack',
        duration:1000,
        complete: function(){
            document.querySelector('.contain.introduccion').style.display = 'none';
            anime({
                targets: '.contain.instrucciones',
                scale: [0,1],
                easing: 'easeOutBack',
                duration: 1000,
                begin: function(){
                    document.querySelector('.contain.instrucciones').style.display = 'flex';
                }
            });
        },
        begin: function(){
            playButton.play();
        }
    });
});
/* Clic  para mostrar instrucciones */
document.getElementsByClassName('entendido')[0].addEventListener("click",()=>{
    anime({
        targets:'.contain.instrucciones',
        scale: [1,0],
        easing: 'easeInBack',
        duration:1000,
        complete: function(){
            document.querySelector('.contain.instrucciones').style.display = 'none';
            anime({
                targets: '.plecaContain',
                opacity: [1,0],
                easing: 'linear',
                duration: 500,
                complete: function(){
                    document.querySelector('.plecaContain').style.display = 'none';
                    // readyScene();
                }
            });
        },
        begin: function(){
            playButton.play();
        }
    })
});

function animateCounter(targetValue) {
    anime({
        targets: '#progreso',
        innerHTML: [$('#progreso').text(), targetValue], // Animar desde 0 hasta el valor deseado
        round: 1, // Redondear los números a números enteros
        easing: 'linear', // Tipo de interpolación lineal para un aumento constante
        duration: 2000 // Duración de la animación en milisegundos (ajusta según tus preferencias)
    });
}

// Llama a la función para iniciar la animación con el valor 100 (puedes cambiarlo según tu necesidad)
// anime({
//     targets: '#palomita',
//     innerHTML: [$('#progreso').text(), targetValue], // Animar desde 0 hasta el valor deseado
//     round: 1, // Redondear los números a números enteros
//     easing: 'linear', // Tipo de interpolación lineal para un aumento constante
//     duration: 2000 // Duración de la animación en milisegundos (ajusta según tus preferencias)
// });