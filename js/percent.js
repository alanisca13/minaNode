var storedIDs;
// comunidadeStorage
// medioambientStorage
// minStorage
// procesamientStorage
// puertStorage
const animateElement = (idElement) => {
    anime({
        targets: document.getElementById(idElement).getAttribute('scale'),
        x: 1, // Escala en el eje X
        y: 1, // Escala en el eje Y
        z: 1, // Escala en el eje Z
        easing: 'easeOutBack', // Tipo de interpolación
        duration: 1000 // Duración de la animación en milisegundos (ajusta según tus preferencias)
    });
}
const animateElements = () => {
    storedIDs = JSON.parse(localStorage.getItem('storedIDs')) || [];
    animateCounter( Math.trunc((storedIDs.length*100)/78) );
    storedIDs.forEach(function (id) {
        if (document.getElementById(id)) {
            animateElement(id);
        }
    });
}
const isIDStored = (id) => {
    return storedIDs.includes(id);
}
const handleClick = (id) => {
    // Verifica si el ID ya está almacenado antes de guardarlo
    if (!isIDStored(id)) {
        // Almacena el ID en LocalStorage
        storedIDs.push(id);
        localStorage.setItem('storedIDs', JSON.stringify(storedIDs));
        // Lanza la animación
        animateElement(id);
        animateCounter( Math.trunc((storedIDs.length*100)/78) );
    }
}
