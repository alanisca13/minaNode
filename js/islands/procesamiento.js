const procesamientoArray = {
    "procesamiento":{
        "name":"Visita Trituración",
        "positionCamera":1.6,
        "rotateCamera":80,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<a-entity id="procesamiento-assets">',
                '<img id="procesamiento_bg" src="assets/islas/procesamiento/procesamiento/images/bg.jpeg" crossorigin="anonymous">',
                '<video  id="procesamiento_persona01" loop="false" src="assets/islas/procesamiento/procesamiento/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>',
                '<video  id="procesamiento_persona02" loop="false" src="assets/islas/procesamiento/procesamiento/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>',
                '<video  id="procesamiento_loop" loop="true"  src="assets/islas/procesamiento/procesamiento/videos/loop.mp4"        preload="auto" crossorigin="anonymous" class="loopVideo" webkit-playsinline playsinline></video>',
        
                '<img id="procesamiento_btn1" src="assets/islas/procesamiento/procesamiento/images/btn1.png" crossorigin="anonymous">',
                '<img id="procesamiento_btn1_press" src="assets/islas/procesamiento/procesamiento/images/btn1_press.png" crossorigin="anonymous">',
                '<img id="procesamiento_btn2" src="assets/islas/procesamiento/procesamiento/images/btn2.png" crossorigin="anonymous">',
                '<img id="procesamiento_btn2_press" src="assets/islas/procesamiento/procesamiento/images/btn2_press.png" crossorigin="anonymous">',
                '<img id="procesamiento_tema" src="assets/islas/procesamiento/procesamiento/images/tema.png" crossorigin="anonymous">',
            '</a-entity>'
        ],
        "elements":[
            `<a-entity id="opciones" position="-6 1.5 .6" scale=".6 .6 .6" rotation="0 80 0">
                <a-image id="pleca" src="#procesamiento_tema" width="1.18" height="1" geometry="width: 8.03; height: 1.93"></a-image>
                <a-image id="btn01" id-video="procesamiento_persona01" src="#procesamiento_btn1" id-src="procesamiento_btn1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="-2.5 -1 0.2" btn-click="Video">
                    <a-image id="procesamiento_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 0.6 0.4"></a-image>
                </a-image>
                <a-image id="btn02" id-video="procesamiento_persona02" src="#procesamiento_btn2" id-src="procesamiento_btn2" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="2.5 -1 0.2" btn-click="Video">
                    <a-image id="procesamiento_persona02_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 0.6 0.4"></a-image>
                </a-image>
            </a-entity>`,
            '<a-videosphere id="persona" src="#procesamiento_persona01" geometry="radius: 99; phiLength: 52.12; thetaLength: 81.87; thetaStart: 64.07; phiStart: 189.02"></a-videosphere>',
            '<a-videosphere id="banda" src="#procesamiento_loop" geometry="radius: 99.5; phiLength: 133.08; thetaLength: 57.37; thetaStart: 93.44; phiStart: 48.68"></a-videosphere>',
            `<a-entity position="-10 5.77 4.17341" rotation="0 95 0">
                <a-image src="#molienda_liga" id-src="molienda_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="procesamiento/molienda">
                    <a-image id="procesamiento/molienda_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.6 1.2 0.4"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="-10 7 -4.2" rotation="0 80 0">
                <a-image src="#relaves_liga" id-src="relaves_liga" width="1.18" height="1" geometry="width: 5.17; height: 3.18" btn-click="Liga" liga="procesamiento/relaves" material="" class="activeVR">
                    <a-image id="procesamiento/relaves_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.3 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            '<a-sky radius="100" id="skyIsland" src="#procesamiento_bg"></a-sky>'
        ]
    },
    "molienda":{
        "name":"Visita la Molienda",
        "positionCamera":1.6,
        "rotateCamera":260,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="molienda_bg" src="assets/islas/procesamiento/molienda/images/bg.jpeg"></img>',
            '<video  id="molienda_persona01" loop="false" src="assets/islas/procesamiento/molienda/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="molienda_loop01" loop="true" src="assets/islas/procesamiento/molienda/videos/loop01.mp4" preload="auto" crossorigin="anonymous" class="loopVideo" webkit-playsinline playsinline> </video>',
            '<video  id="molienda_loop02" loop="true" src="assets/islas/procesamiento/molienda/videos/loop02.mp4" preload="auto" crossorigin="anonymous" class="loopVideo" webkit-playsinline playsinline> </video>',
            '<video  id="molienda_loop03" loop="true" src="assets/islas/procesamiento/molienda/videos/loop03.mp4" preload="auto" crossorigin="anonymous" class="loopVideo" webkit-playsinline playsinline> </video>',
            
            '<img id="molienda_tema" src="assets/islas/procesamiento/molienda/images/tema.png">',
            '<img id="molienda_btn1" src="assets/islas/procesamiento/molienda/images/btn1.png">',
            '<img id="molienda_btn1_press" src="assets/islas/procesamiento/molienda/images/btn1_press.png">'
        ],
        "elements":[
            `
            <a-entity id="opciones" position="5.8 0.3 3" scale="0.6 0.6 0.6" rotation="0 262.6 0" >
                <a-image id="pleca" src="#molienda_tema" width="1.18" height="1" geometry="width: 8.03; height: 1.93" material=""></a-image>
                <a-image id="btn01" src="#molienda_btn1" id-src="molienda_btn1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="0 -0.7 0.5" btn-click="Video" id-video="molienda_persona01" material="" text__molienda="">
                    <a-image id="molienda_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 0.6 0.4"></a-image>
                </a-image>
            </a-entity>
            <a-videosphere id="persona" src="#molienda_persona01" geometry="radius: 98; phiLength: 33.2; thetaLength: 73.6; thetaStart: 72.5; phiStart: 333.7" visible="" ></a-videosphere>
            <a-videosphere id="loop01_V" src="#molienda_loop01" geometry="radius: 99; phiLength: 25.8; thetaLength: 21.6; thetaStart: 86.5; phiStart: 2.6" visible="" material="" ></a-videosphere>
            <a-videosphere id="loop02_V" src="#molienda_loop02" geometry="radius: 99; phiLength: 26.64; thetaLength: 16.1; thetaStart: 84.6; phiStart: 310.66" visible="" material="" ></a-videosphere>
            <a-videosphere id="loop03_V" src="#molienda_loop03" geometry="radius: 99; phiLength: 41.3; thetaLength: 43.6; thetaStart: 84.3; phiStart: 38.3" visible="" material="" ></a-videosphere>
        
            <a-entity position="8.5 5.8 -2" rotation="0 270 0">
                <a-image src="#procesamiento_liga" id-src="procesamiento_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="procesamiento/procesamiento">
                    <a-image id="procesamiento/procesamiento_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.6 1.2 0.4"></a-image>
                </a-image>
            </a-entity>
            <a-entity position="8.5 4.2 8.5" rotation="0 235 0">
                <a-image src="#flotacion_liga" id-src="flotacion_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="procesamiento/flotacion">
                    <a-image id="procesamiento/flotacion_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
                </a-image>
            </a-entity>
            <a-entity position="1.25 2.5 10.6" rotation="0 180 0">
                <a-image src="#centroControl_liga" id-src="centroControl_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="procesamiento/centroControl">
                    <a-image id="procesamiento/centroControl_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.2 0.4"></a-image>
                </a-image>
            </a-entity>
            
            <a-sky radius="100" id="skyIsland" src="#molienda_bg" color=""></a-sky>
            `
        ]
    },
    "flotacion":{
        "name":"Visita la Flotación",
        "positionCamera":1.6,
        "rotateCamera":345,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="flotacion_bg" src="assets/islas/procesamiento/flotacion/images/bg.jpg"></img>',
            '<video  id="flotacion_persona01" loop="false" src="assets/islas/procesamiento/flotacion/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',

            '<video  id="flotacion_loop01" loop="true" src="assets/islas/procesamiento/flotacion/videos/loop01.mp4" preload="auto" class="loopVideo"  crossorigin="anonymous" webkit-playsinline playsinline></video>',
            '<video  id="flotacion_loop02" loop="true" src="assets/islas/procesamiento/flotacion/videos/loop02.mp4" preload="auto" class="loopVideo"  crossorigin="anonymous" webkit-playsinline playsinline></video>',
            '<video  id="flotacion_loop03" loop="true" src="assets/islas/procesamiento/flotacion/videos/loop03.mp4" preload="auto" class="loopVideo"  crossorigin="anonymous" webkit-playsinline playsinline></video>',

            '<img id="flotacion_btn1" src="assets/islas/procesamiento/flotacion/images/btn1.png">',
            '<img id="flotacion_btn1_press" src="assets/islas/procesamiento/flotacion/images/btn1_press.png">',
            '<img id="flotacion_tema" src="assets/islas/procesamiento/flotacion/images/tema.png">'
        ],
        "elements":[
            `
            <a-entity id="opciones" position="4.4 0 -5.5" scale="0.6 0.6 0.6" rotation="0 330 0" ="opciones">
                <a-image id="pleca" src="#flotacion_tema" width="1.18" height="1" geometry="width: 8.03; height: 1.93"></a-image>
                <a-image id="btn01" src="#flotacion_btn1" id-src="flotacion_btn1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="0 -1 0.1" btn-click="Video" id-video="flotacion_persona01">
                    <a-image id="flotacion_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 0.6 0.4"></a-image>
                </a-image>
            </a-entity>
            <a-videosphere id="persona" src="#flotacion_persona01" geometry="radius: 98; phiLength: 25; thetaLength: 55.28; thetaStart: 80.45; phiStart: 260.8" visible="" ="" material="color: #ededed"></a-videosphere>
            <a-videosphere id="loop01_V" src="#flotacion_loop01" geometry="radius: 99; phiLength: 59.55; thetaLength: 31.73; thetaStart: 107.4; phiStart: 179.83" visible="" ="" material="color: #ededed"></a-videosphere>
            <a-videosphere id="loop02_V" src="#flotacion_loop02" geometry="radius: 99; phiLength: 32.31; thetaLength: 44.53; thetaStart: 98.86; phiStart: 327.64" visible="" ="" material="color: #dbdbdb"></a-videosphere>
            <a-videosphere id="loop03_V" src="#flotacion_loop03" geometry="radius: 99; phiLength: 35.75; thetaLength: 12.49; thetaStart: 102.67; phiStart: 39.95" visible="" ="" material="color: #ededed"></a-videosphere>
        
            <a-entity position="10.2 3.3 3.7" rotation="0 239 0">
                <a-image src="#seguridad_liga" id-src="seguridad_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="procesamiento/seguridad" material="">
                    <a-image id="procesamiento/seguridad_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.7 1.2 0.4"></a-image>
                </a-image>
            </a-entity>
            <a-entity position="-6.3 4 1.8" rotation="0 110 0">
                <a-image src="#molienda_liga" id-src="molienda_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="procesamiento/molienda" material="">
                    <a-image id="procesamiento/molienda_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.7 1.2 0.4"></a-image>
                </a-image>
            </a-entity>
            <a-entity position="12.2 3 0" rotation="0 285 0">
                <a-image src="#drone_liga" id-src="drone_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12"  btn-click="Liga" liga="procesamiento/drone">
                    <a-image id="procesamiento/drone_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
                </a-image>
            </a-entity>
            <a-entity position="-10 7 -4.2" rotation="0 80 0">
                <a-image src="#relaves_liga" id-src="relaves_liga" width="1.18" height="1" geometry="width: 5.17; height: 3.18" btn-click="Liga" liga="procesamiento/relaves" material="" class="activeVR">
                    <a-image id="procesamiento/relaves_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.3 1.4 0.1"></a-image>
                </a-image>
            </a-entity>
            
            <a-sky radius="100" id="skyIsland" src="#flotacion_bg" color="#f7f7f7"></a-sky>
            `
        ]
    },
    "eppmina":{
        "name":"Explora los equipos de protección personal",
        "positionCamera":1.6,
        "rotateCamera":220,
        "ambiente":true,
        "voz":true,
        "onlyView":false,
        "assets":[
            '<img id="eppmina_bg" src="assets/islas/procesamiento/eppmina/images/bg.jpeg">',
            '<video  id="eppmina_loop01" loop="true" src="assets/islas/procesamiento/eppmina/videos/loop01.mp4" preload="auto" crossorigin="anonymous" class="loopVideo" webkit-playsinline playsinline> </video>',
            '<video  id="eppmina_loop02" loop="true" src="assets/islas/procesamiento/eppmina/videos/loop02.mp4" preload="auto" crossorigin="anonymous" class="loopVideo" webkit-playsinline playsinline> </video>'
        ],
        "elements":[
            `<a-videosphere id="loop01_V" src="#eppmina_loop01" geometry="radius: 99; phiLength: 36.6; thetaLength: 29.2; thetaStart: 81.5; phiStart: 326.36" visible="" material="" ></a-videosphere>
            <a-videosphere id="loop02_V" src="#eppmina_loop02" geometry="radius: 99; phiLength: 97.92; thetaLength: 70.81; thetaStart: 73.71; phiStart: 16.51" visible="" material="" ></a-videosphere>
            
            <a-entity position="-0.8 6.4 13.3" rotation="0 185 0">
                <a-image src="#centroControl_liga" id-src="centroControl_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="procesamiento/centroControl">
                    <a-image id="procesamiento/centroControl_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.2 0.4"></a-image>
                </a-image>
            </a-entity>
            <a-entity position="10.5 2.3 1.6" rotation="0 270 0" visible="">
                <a-image src="#flotacion_liga" id-src="flotacion_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="procesamiento/flotacion">
                    <a-image id="procesamiento/flotacion_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.2 0.4"></a-image>
                </a-image>
            </a-entity>
            <a-entity position="-0.25 3.6 -10.5" rotation="">
                <a-image src="#seguridad_liga" id-src="seguridad_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" position="" btn-click="Liga" liga="procesamiento/seguridad">
                    <a-image id="procesamiento/seguridad_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.2 0.4"></a-image>
                </a-image>
            </a-entity>
            
            <a-sky radius="100" id="skyIsland" src="#eppmina_bg"></a-sky>`
        ]
    },
    "seguridad":{
        "name":"Trabajamos con seguridad",
        "positionCamera":1.6,
        "rotateCamera":150,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="seguridad_bg" src="assets/islas/procesamiento/seguridad/images/bg.jpeg">',
            '<video  id="seguridad_persona01" loop="false" src="assets/islas/procesamiento/seguridad/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="seguridad_persona02" loop="false" src="assets/islas/procesamiento/seguridad/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="loop01" loop="true" src="assets/islas/procesamiento/seguridad/videos/loop01.mp4" preload="auto" crossorigin="anonymous" class="loopVideo" webkit-playsinline playsinline> </video>',

            '<img id="seguridad_btn1" src="assets/islas/procesamiento/seguridad/images/btn1.png">',
            '<img id="seguridad_btn1_press" src="assets/islas/procesamiento/seguridad/images/btn1_press.png">',
            '<img id="seguridad_btn2" src="assets/islas/procesamiento/seguridad/images/btn2.png">',
            '<img id="seguridad_btn2_press" src="assets/islas/procesamiento/seguridad/images/btn2_press.png">',
            '<img id="seguridad_tema" src="assets/islas/procesamiento/seguridad/images/tema.png">'
        ],
        "elements":[
            `
            <a-entity id="opciones" position="-0.9 1.5 5.6" scale="0.6 0.6 0.6" rotation="0 155.95 0" ="">
                <a-image id="pleca" src="#seguridad_tema" width="1.18" height="1" geometry="width: 8.03; height: 1.93" material=""></a-image>
                
                <a-image id="btn01" src="#seguridad_btn1" id-src="seguridad_btn1" width="1.18" height="1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="-2.5 -1 0.2" btn-click="Video" id-video="seguridad_persona01">
                    <a-image id="seguridad_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.7 0.6 0.4"></a-image>
                </a-image>
                <a-image id="btn02" src="#seguridad_btn2" id-src="seguridad_btn2" width="1.18" height="1" width="1.18" height="1" geometry="width: 5.59; height: 1.1" position="2.5 -1 0.2" btn-click="Video" id-video="seguridad_persona02">
                    <a-image id="seguridad_persona02_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.7 0.6 0.4"></a-image>
                </a-image>
            </a-entity>
            
            <a-videosphere id="persona" src="#seguridad_persona01" geometry="radius: 98.5; phiLength: 35.53; thetaLength: 76.36; thetaStart: 67.42; phiStart: 138.44" material="color: #ffffff"></a-videosphere>
            <a-videosphere id="loop01_V" src="#loop01" geometry="radius: 99; phiLength: 41.52; thetaLength: 50.58; thetaStart: 78.4; phiStart: 176.6" material=""></a-videosphere>
        
            <a-entity position="-6.5 3.5 6" rotation="0 150 0">
                <a-image src="#molienda_liga" id-src="molienda_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="procesamiento/molienda">
                    <a-image id="procesamiento/molienda_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.2 0.4"></a-image>
                </a-image>
            </a-entity>
            <a-entity position="4.7 2.3 6.1" rotation="0 220 0">
                <a-image src="#eppmina_liga" id-src="eppmina_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" position="" btn-click="Liga" liga="procesamiento/eppmina">
                    <a-image id="procesamiento/eppmina_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.85 1.2 0.4"></a-image>
                </a-image>
            </a-entity>
            <a-sky radius="100" id="skyIsland" src="#seguridad_bg"></a-sky>
            `
        ]
    },
    "areanorte":{
        "name":"Vuela sobre el área norte del TMF",
        "positionCamera":0,
        "rotateCamera":230,
        "ambiente":false,//falta agregar audio de medio ambiente
        "voz":true,
        "onlyView":true,
        "assets":[
            '<video id="areaNorte_loop01" loop="true" src="assets/islas/procesamiento/areanorte/videos/loop01.mp4" preload="auto" crossorigin="anonymous" class="loopVideo" webkit-playsinline playsinline></video>'
        ],
        "elements":[
            '<a-videosphere id="areaNorte_loop01_V" src="#areaNorte_loop01" geometry="radius: 99;"></a-videosphere>'
        ]
    },
    "centroControl":{
        "name":"Explora el centro de control",
        "positionCamera":0,
        "rotateCamera":90,
        "ambiente":true,
        "voz":true,
        "onlyView":true,
        "assets":[
            '<img id="centroControl_bg" src="assets/islas/procesamiento/centroControl/images/bg.jpg" >',
            '<video  id="centroControl_loop01" loop="true" src="assets/islas/procesamiento/centroControl/videos/loop01.mp4" preload="auto" crossorigin="anonymous" class="loopVideo" webkit-playsinline playsinline></video>',
            '<video  id="centroControl_loop02" loop="true" src="assets/islas/procesamiento/centroControl/videos/loop02.mp4" preload="auto" crossorigin="anonymous" class="loopVideo" webkit-playsinline playsinline></video>',
            '<video  id="centroControl_loop03" loop="true" src="assets/islas/procesamiento/centroControl/videos/loop03.mp4" preload="auto" crossorigin="anonymous" class="loopVideo" webkit-playsinline playsinline></video>'
        ],
        "elements":[
            `<a-videosphere id="loop01_V" src="#centroControl_loop01" geometry="radius: 99; phiLength: 134.15; thetaLength: 25.02; thetaStart: 89.41; phiStart: 96.68" material="color: #d6d6d6"></a-videosphere>
            <a-videosphere id="loop02_V" src="#centroControl_loop02" geometry="radius: 99; phiLength: 50.47; thetaLength: 39.83; thetaStart: 82.98; phiStart: 244.2" material="color: #d6d6d6" ></a-videosphere>
            <a-videosphere id="loop03_V" src="#centroControl_loop03" geometry="radius: 99; phiLength: 20.19; thetaLength: 26.85; thetaStart: 90.22; phiStart: 72.17" material="color: #d6d6d6" ></a-videosphere>
            <a-sky radius="100" id="skyIsland" src="#centroControl_bg"></a-sky>`
        ]
    },
    "drone":{
        "name":"Vuela sobre la planta de procesos",
        "positionCamera":0,
        "rotateCamera":90,
        "ambiente":false,//falta agregar audio de medio ambiente
        "voz":true,
        "onlyView":true,
        "assets":[
            '<video  id="drone_loop01" loop="true" src="assets/islas/procesamiento/drone/videos/loop01.mp4" preload="auto" crossorigin="anonymous" class="loopVideo" webkit-playsinline playsinline></video>'
        ],
        "elements":[
            `<a-videosphere id="drone_loop01_V" src="#drone_loop01" geometry="radius: 99;"></a-videosphere>`
        ]
    },
    "relaves":{
        "name":"Explora el Manejo de Relaves de Cobre Panamá",
        "positionCamera":1.6,
        "rotateCamera":50,
        "ambiente":true,
        "voz":true,
        "onlyView":false,
        "assets":[
            '<img id="relaves_bg" src="assets/islas/procesamiento/relaves/images/bg.jpg"></img>'
        ],
        "elements":[
            `<a-entity position="-10 5.77 4.17341" rotation="0 95 0">
                <a-image src="#molienda_liga" id-src="molienda_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="procesamiento/molienda">
                    <a-image id="procesamiento/molienda_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.6 1.2 0.4"></a-image>
                </a-image>
            </a-entity>`,
            `
            <a-entity position="-3 3.3 -15" rotation="0 13 0">
                <a-image src="#areanorte_liga" id-src="areanorte_liga" width="1.18" height="1" geometry="width: 3.93; height: 3.13" btn-click="Liga" liga="procesamiento/areanorte" material="" class="">
                    <a-image id="procesamiento/areanorte_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.7 1.5 0.1"></a-image>
                </a-image>
            </a-entity>
            <a-entity position="12 6.5 15.5" rotation="0 225 0">
                <a-image src="#vuelarelave_liga" id-src="vuelarelave_liga" width="1.18" height="1" geometry="width: 6.18; height: 3.12" material="" btn-click="Liga" liga="procesamiento/vuelarelave" class="">
                    <a-image id="procesamiento/vuelarelave_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.9 1.5 0.1"></a-image>
                </a-image>
            </a-entity>
            
            <a-sky radius="100" id="skyIsland" src="#relaves_bg"></a-sky>
            `
        ]
    },
    "vuelarelave":{
        "name":"Vuela sobre el área norte del TMF",
        "positionCamera":0,
        "rotateCamera":180,
        "ambiente":false,
        "voz":true,
        "onlyView":true,
        "assets":[
            '<video id="vuelarelave_loop01" loop="true" src="assets/islas/procesamiento/vuelarelave/videos/loop01.mp4" preload="auto" crossorigin="anonymous" class="loopVideo" webkit-playsinline playsinline></video>'
        ],
        "elements":[
            `<a-videosphere id="loop01_V" src="#vuelarelave_loop01" geometry="radius: 99;"></a-videosphere>`
        ]
    }
}