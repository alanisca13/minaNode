const medioambienteArray = {
    "lab":{
        "name":"Descubre la ciencia ambiental de Cobre Panamá",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":0,
        "ambiente":false,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="lab_bg" src="assets/islas/medioambiente/lab/images/bg.jpeg" crossorigin="anonymous">',
            '<video  id="lab_persona01" loop="true" src="assets/islas/medioambiente/lab/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<img id="lab_btn1" src="assets/islas/medioambiente/lab/images/btn1.png" crossorigin="anonymous">',
            '<img id="lab_btn1_press" src="assets/islas/medioambiente/lab/images/btn1_press.png" crossorigin="anonymous">',
            '<img id="lab_pleca" src="assets/islas/medioambiente/lab/images/pleca.png" crossorigin="anonymous">'
        ],
        "elements":[
            `<a-entity id="opciones" position="-7 0 -8.2" scale="1 1 1" rotation="-15 60 0">
                <a-image id="pleca" src="#lab_pleca" width="1.18" height="1" geometry="width: 8.5; height: 1.55"></a-image>
                <a-image id="btn01" id-video="lab_persona01" src="#lab_btn1" id-src="lab_btn1" width="1.18" height="1" geometry="width: 6.63; height: 1.1" position="0 -.7 0.2" btn-click="Video">
                    <a-image id="lab_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.3 0.5 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-videosphere id="persona" src="#lab_persona01" geometry="radius: 99; phiLength: 33.43; thetaLength: 66.96; thetaStart: 72.59; phiStart: 163.28"></a-videosphere>`,

            `<a-entity position="0.4 9 -12.5" rotation="0 0 0">
                <a-image src="#reforestacion_liga" id-src="reforestacion_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="medioambiente/reforestacion" >
                    <a-image id="medioambiente/reforestacion_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="-0.6 9 10" rotation="0 170 0">
                <a-image src="#nursery_liga" id-src="nursery_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="medioambiente/nursery">
                    <a-image id="medioambiente/nursery_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.7 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="-9.6 7.2 -8.9" rotation="0 75 0">
                <a-image src="#playa_liga" id-src="playa_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="medioambiente/playa">
                    <a-image id="medioambiente/playa_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
                </a-image>
            </a-entity>`,
            `<a-sky radius="100" src="#lab_bg" color=""></a-sky>`
        ]
    },
    "reforestacion":{
        "name":"Rehabilitando el bosque",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":0,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="reforestacion_bg" src="assets/islas/medioambiente/reforestacion/images/bg.jpg">',
            '<video  id="reforestacion_persona01" loop="false" src="assets/islas/medioambiente/reforestacion/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<img id="reforestacion_btn1" src="assets/islas/medioambiente/reforestacion/images/btn1.png">',
            '<img id="reforestacion_btn1_press" src="assets/islas/medioambiente/reforestacion/images/btn1_press.png">',
            '<img id="reforestacion_pleca" src="assets/islas/medioambiente/reforestacion/images/pleca.png">'
        ],
        "elements":[
            `<a-entity id="opciones" position="-13 0 -8.2" scale="1 1 1" rotation="0 60 0" load-obj="opciones">
                <a-image id="pleca" src="#reforestacion_pleca" width="1.18" height="1" geometry="width: 8.02; height: 1.54"></a-image>
                <a-image id="btn01" id-video="reforestacion_persona01" src="#reforestacion_btn1" id-src="reforestacion_btn1" width="1.18" height="1" geometry="width: 6.73; height: 1.1" position="0 -0.7 0.2" btn-click="Video">
                    <a-image id="reforestacion_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.3 0.6 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="-5.5 5 7.5" rotation="0 140 0">
                <a-image src="#playa_liga" id-src="playa_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="medioambiente/playa" material="">
                    <a-image id="medioambiente/playa_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.7 1.2 0.4"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="12.2 5 0" rotation="0 275 0">
                <a-image src="#pasoFauna_liga" id-src="pasoFauna_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12"  btn-click="Liga" liga="medioambiente/pasosfauna">
                    <a-image id="medioambiente/pasosfauna_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
                </a-image>
            </a-entity>`,
            `<a-videosphere id="persona" src="#reforestacion_persona01" geometry="radius: 99; phiLength: 37.07; thetaLength: 65.83; thetaStart: 77; phiStart: 155.82" visible="" load-obj="" material=""></a-videosphere>`,
            `<a-sky radius="100" src="#reforestacion_bg" color="#e7edf3" load-obj></a-sky>`
        ]
    },
    "nursery":{
        "name":"Visita nuestro vivero",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":150,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="nursery_bg" src="assets/islas/medioambiente/nursery/images/bg.jpeg">',
            '<video id="nursery_persona01" loop="false" src="assets/islas/medioambiente/nursery/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video id="nursery_persona02" loop="false" src="assets/islas/medioambiente/nursery/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<img id="nursery_btn1" src="assets/islas/medioambiente/nursery/images/btn1.png">',
            '<img id="nursery_btn1_press" src="assets/islas/medioambiente/nursery/images/btn1_press.png">',
            '<img id="nursery_btn2" src="assets/islas/medioambiente/nursery/images/btn2.png">',
            '<img id="nursery_btn2_press" src="assets/islas/medioambiente/nursery/images/btn2_press.png">',
            '<img id="nursery_pleca" src="assets/islas/medioambiente/nursery/images/pleca.png">'
        ],
        "elements":[
            `<a-entity id="opciones" position="-7.75 0 7" scale="1 1 1" rotation="-15 140 0" load-obj="opciones">
                <a-image id="pleca" src="#nursery_pleca" width="1.18" height="1" geometry="width: 7.57; height: 2.08"></a-image>
                <a-image id="btn01" id-video="nursery_persona01" src="#nursery_btn1" id-src="nursery_btn1" width="1.18" height="1" geometry="width: 3.96; height: 1.1" position="-3 -1 0.2" btn-click="Video">
                    <a-image id="nursery_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2 0.6 0.1"></a-image>
                </a-image>
                <a-image id="btn02" id-video="nursery_persona02" src="#nursery_btn2" id-src="nursery_btn2" width="1.18" height="1" geometry="width: 6.63; height: 1.1" position="2.3 -1 0.2" btn-click="Video">
                    <a-image id="nursery_persona02_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.2 0.6 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="-5 7 10" rotation="0 160 0">
                <a-image src="#pitoffice_liga" id-src="pitoffice_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="medioambiente/pitoffice" material="" class="">
                    <a-image id="medioambiente/pitoffice_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.5 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="4.1 6 7.8" rotation="0 200 0">
                <a-image src="#lab_liga" id-src="lab_liga" width="1.18" height="1" geometry="width: 6.31; height: 3.12" btn-click="Liga" liga="medioambiente/lab" material="" class="">
                    <a-image id="medioambiente/lab_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.01 1.5 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="0 7 -10" rotation="0 0 0">
                <a-image src="#rio_liga" id-src="rio_liga" width="1.18" height="1" geometry="width: 5.63; height: 3.12" btn-click="Liga" liga="medioambiente/rio" material="" class="activeVR" position="">
                    <a-image id="medioambiente/rio_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.7 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-videosphere id="persona" src="#nursery_persona02" geometry="radius: 99; phiLength: 50.03; thetaLength: 85.67; thetaStart: 64.01; phiStart: 66.2" visible="" load-obj="" material=""></a-videosphere>`,
            `<a-sky radius="100" src="#nursery_bg"  load-obj></a-sky>`
        ]
    },
    "pasosfauna":{
        "name":"Explora un paso de fauna",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":90,
        "ambiente":true,
        "voz":true,
        "onlyView":true,
        "assets":[
            '<video id="pasosfauna_loop01" loop="true" src="assets/islas/medioambiente/pasosfauna/videos/bg.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>'
        ],
        "elements":[
            '<a-videosphere id="loop01_V" src="#pasosfauna_loop01" geometry="radius: 99;"></a-videosphere>'
        ]
    },
    "pitoffice":{
        "name":"Conoce nuestra filosofía ambiental",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":70,
        "ambiente":false,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="pitoffice_bg" src="assets/islas/medioambiente/pitoffice/images/bg.jpeg">',
            '<video  id="pitoffice_persona01" loop="false" src="assets/islas/medioambiente/pitoffice/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="pitoffice_persona02" loop="false" src="assets/islas/medioambiente/pitoffice/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<img id="pitoffice_btn1" src="assets/islas/medioambiente/pitoffice/images/btn1.png">',
            '<img id="pitoffice_btn1_press" src="assets/islas/medioambiente/pitoffice/images/btn1_press.png">',
            '<img id="pitoffice_btn2" src="assets/islas/medioambiente/pitoffice/images/btn2.png">',
            '<img id="pitoffice_btn2_press" src="assets/islas/medioambiente/pitoffice/images/btn2_press.png">',
            '<img id="pitoffice_pleca" src="assets/islas/medioambiente/pitoffice/images/pleca.png">'
        ],
        "elements":[
            `<a-entity id="opciones" position="-7 0 -8.2" scale="1 1 1" rotation="-15 55 0" load-obj="opciones">
                <a-image id="pleca" src="#pitoffice_pleca" width="1.18" height="1" geometry="width: 11.02; height: 1.55"></a-image>
                <a-image id="btn01" id-video="pitoffice_persona01" src="#pitoffice_btn1" id-src="pitoffice_btn1" width="1.18" height="1" geometry="width: 4.78; height: 1.1" position="-3.4 -0.65 0.2" btn-click="Video">
                    <a-image id="pitoffice_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.4 0.6 0.1"></a-image>
                </a-image>
                <a-image id="btn02" id-video="pitoffice_persona02" src="#pitoffice_btn2" id-src="pitoffice_btn2" width="1.18" height="1" geometry="width: 6.63; height: 1.1" position="2.5 -0.65 0.2" btn-click="Video">
                    <a-image id="pitoffice_persona02_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.3 0.6 0.1"></a-image>
                </a-image>
            </a-entity>`,

            `<a-entity position="-5 7 10" rotation="0 140 0">
                <a-image src="#playa_liga" id-src="playa_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="medioambiente/playa" material="" class="">
                    <a-image id="medioambiente/playa_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.5 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="3 6 10" rotation="0 200 0">
                <a-image src="#nursery_liga" id-src="nursery_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="medioambiente/nursery" material="" class="">
                    <a-image id="medioambiente/nursery_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.5 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="0 7 -10" rotation="0 20 0">
                <a-image src="#rio_liga" id-src="rio_liga" width="1.18" height="1" geometry="width: 5.63; height: 3.12" btn-click="Liga" liga="medioambiente/rio">
                    <a-image id="medioambiente/rio_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.7 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,

            `<a-videosphere id="persona" src="#pitoffice_persona01" geometry="radius: 99; phiLength: 48.810; thetaLength: 87.130; thetaStart: 55.790; phiStart: 152.21" visible="" load-obj="" material=""></a-videosphere>`,
            `<a-sky radius="100" src="#pitoffice_bg" load-obj></a-sky>`,
        ]
    },
    "playa":{
        "name":"¿Qué significa minería ambientalmente responsable?",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":280,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="playa_bg" src="assets/islas/medioambiente/playa/images/bg.jpg">',
            '<video  id="playa_persona01" loop="false" src="assets/islas/medioambiente/playa/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<img id="playa_btn1" src="assets/islas/medioambiente/playa/images/btn1.png">',
            '<img id="playa_btn1_press" src="assets/islas/medioambiente/playa/images/btn1_press.png">',
            '<img id="playa_pleca" src="assets/islas/medioambiente/playa/images/pleca.png">'
        ],
        "elements":[
            `<a-entity id="opciones" position="8 0 -5.5" scale="1 1 1" rotation="-10 300 0" load-obj="opciones">
                <a-image id="pleca" src="#playa_pleca" width="8.5" height="1.54"></a-image>
                <a-image id="btn01" id-video="playa_persona01" src="#playa_btn1" id-src="playa_btn1" width="6.13" height="1.1" position="0 -0.65 0.2" btn-click="Video">
                    <a-image id="playa_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3 0.4 .1"></a-image>
                </a-image>
            </a-entity>`,

            `<a-videosphere id="persona" src="#playa_persona01" geometry="radius: 99; phiLength: 43.46; thetaLength: 68.24; thetaStart: 77.36; phiStart: 349.02" visible="" load-obj="" material=""></a-videosphere>`,
            `<a-entity position="3.5 7 -11.6" rotation="0 -25 0">
                <a-image src="#pasosfauna_liga" id-src="pasosfauna_liga" width="1.18" height="1" geometry="width: 3.93; height: 3.13" btn-click="Liga" liga="medioambiente/pasosfauna" material="" class="">
                    <a-image id="medioambiente/pasosfauna_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.7 1.5 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="-4 6 -11.6" rotation="0 30 0">
                <a-image src="#reforestacion_liga" id-src="reforestacion_liga" width="1.18" height="1" geometry="width: 3.92; height: 3.12" material="" btn-click="Liga" liga="medioambiente/reforestacion" class="">
                    <a-image id="medioambiente/reforestacion_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.5 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="-9.6 7 -8.9" rotation="0 42.4 0">
                <a-image src="#pitoffice_liga" id-src="pitoffice_liga" width="1.18" height="1" geometry="width: 4.82; height: 3.12" material="" btn-click="Liga" liga="medioambiente/pitoffice" class="">
                <a-image id="medioambiente/pitoffice_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.2 1.5 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-sky radius="100" src="#playa_bg" load-obj></a-sky>`
        ]
    },
    "rio":{
        "name":"Vuela sobre los ríos de las comunidades mineras",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":300,
        "ambiente":true,
        "voz":true,
        "onlyView":false,
        "assets":[
            '<video  id="rio_loop01" loop="true" src="assets/islas/medioambiente/rio/videos/bg.mp4" class="loopVideo" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>'
        ],
        "elements":[
            '<a-videosphere id="loop01_V" src="#rio_loop01" geometry="radius: 99;"></a-videosphere>'
        ]
    }
}