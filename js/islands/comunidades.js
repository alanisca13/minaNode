const comunidadesArray = {
    "caminos":{
        "name":"Caminos que cambian vidas",
        "positionCamera":0,
        "positionCamera":1.6,
        "rotateCamera":75,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="caminos_bg" src="assets/islas/comunidades/caminos/images/bg.jpg">',
            '<video  id="caminos_loop01" loop="true" src="assets/islas/comunidades/caminos/videos/loop01.mp4" preload="auto" crossorigin="anonymous" class="loopVideo" webkit-playsinline playsinline></video>',
            '<!-- PERSONA(S) -->',
            '<video  id="caminos_persona01" loop="false" src="assets/islas/comunidades/caminos/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="caminos_persona02" loop="false" src="assets/islas/comunidades/caminos/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="caminos_persona03" loop="false" src="assets/islas/comunidades/caminos/videos/persona03.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<!-- OPCION MULTIPLE -->',
            '<img id="caminos_btn01" src="assets/islas/comunidades/caminos/images/btn1.png">',
            '<img id="caminos_btn01_press" src="assets/islas/comunidades/caminos/images/btn1_press.png">',
            '<img id="caminos_btn02" src="assets/islas/comunidades/caminos/images/btn2.png">',
            '<img id="caminos_btn02_press" src="assets/islas/comunidades/caminos/images/btn2_press.png">',
            '<img id="caminos_btn03" src="assets/islas/comunidades/caminos/images/btn3.png">',
            '<img id="caminos_btn03_press" src="assets/islas/comunidades/caminos/images/btn3_press.png">',
            '<img id="caminos_pleca" src="assets/islas/comunidades/caminos/images/pleca.png">'
        ],
        "elements":[
            `<a-entity id="opciones" position="-8.75 1 -6" scale="1 1 1" rotation="0 60 0" load-obj="opciones">
                <a-image id="pleca" src="#caminos_pleca" width="1.18" height="1" geometry="width: 8.03; height: 1.93"></a-image>
                <a-image id="btn01" id-video="caminos_persona01" src="#caminos_btn01" id-src="caminos_btn01" width="1.18" height="1" geometry="width: 4.99; height: 1.1" position="-5.37 -1 0.2" btn-click="Video" material="">
                    <a-image id="caminos_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.4 0.6 0.1"></a-image>
                </a-image>
                <a-image id="btn02" id-video="caminos_persona02" src="#caminos_btn02" id-src="caminos_btn02" width="1.18" height="1" geometry="width: 5.64; height: 1.1" position=".1 -1 0.2" btn-click="Video" >
                    <a-image id="caminos_persona02_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.7 0.6 0.1"></a-image>
                </a-image>
                <a-image id="btn03" id-video="caminos_persona03" src="#caminos_btn03" id-src="caminos_btn03" width="1.18" height="1" geometry="width: 4.99; height: 1.1" position="5.600 -1 0.2" btn-click="Video" >
                    <a-image id="caminos_persona03_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.4 0.6 0.1"></a-image>
                </a-image>
            </a-entity>`,

            `<a-entity position="-6.3 5 7.6" rotation="0 130 0">
                <a-image src="#iptcoclecito_liga" id-src="iptcoclecito_liga" width="1.18" height="1" geometry="width: 5.21; height: 3.13" btn-click="Liga" liga="comunidades/iptcoclecito">
                    <a-image id="comunidades/iptcoclecito_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.5 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="0.4 4.5 -9" rotation="0 0 0">
                <a-image src="#testimonio_liga" id-src="testimonio_liga" width="1.18" height="1" geometry="width: 5.21; height: 3.13" btn-click="Liga" liga="comunidades/testimonio">
                    <a-image id="comunidades/testimonio_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.5 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="9 5 2.5" rotation="0 255 0">
                <a-image src="#ceibafarm_liga" id-src="ceibafarm_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="comunidades/ceibafarm">
                    <a-image id="comunidades/ceibafarm_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.85 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-videosphere id="loop01_V" src="#caminos_loop01" geometry="radius: 99; phiLength: 66.5; thetaLength: 32.3; thetaStart: 76.7; phiStart: 43.5" material="color: #ffffff; opacity: 0.54"></a-videosphere>`,
            `<a-videosphere id="persona" src="#caminos_persona01" geometry="radius: 99; phiLength: 26.9; thetaLength: 50.3; thetaStart: 74.33; phiStart: 145.98" visible="" load-obj="" material=""></a-videosphere>`,
            
            `<a-sky radius="100" src="#caminos_bg" load-obj></a-sky>`
        ]
    },
    "ceibafarm":{
        "name":"Vuela sobre granos de café",
        "positionCamera":0,
        "rotateCamera":0,
        "ambiente":false,
        "voz":true,
        "onlyView":true,
        "assets":[
            '<video  id="ceibafarm_loop01" loop="true" src="assets/islas/comunidades/ceibafarm/videos/bg.mp4" preload="auto" crossorigin="anonymous" class="loopVideo" webkit-playsinline playsinline></video>'
        ],
        "elements":[
            `<a-videosphere id="skyIsland" src="#ceibafarm_loop01" geometry="radius: 99;"></a-videosphere>`
        ]
    },
    "defensoreshidricos":{
        "name":"Una comunidad que protege los ríos",
        "positionCamera":1.6,
        "rotateCamera":40,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="defensoreshidricos_bg" src="assets/islas/comunidades/defensoreshidricos/images/bg.jpeg"></img>',
            '<video  id="defensoreshidricos_persona01" loop="false" src="assets/islas/comunidades/defensoreshidricos/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',

            '<img id="defensoreshidricos_btn1" src="assets/islas/comunidades/defensoreshidricos/images/btn1.png">',
            '<img id="defensoreshidricos_btn1_press" src="assets/islas/comunidades/defensoreshidricos/images/btn1_press.png">',
            '<img id="defensoreshidricos_pleca" src="assets/islas/comunidades/defensoreshidricos/images/pleca.png">'
        ],
        "elements":[
            `<a-entity id="opciones" position="-13.5 0 -4" scale="1 1 1" rotation="-15 70 0" load-obj="opciones">
                <a-image id="pleca" src="#defensoreshidricos_pleca" width="1.18" height="1" geometry="width: 8.5; height: 1.55" material=""></a-image>
                <a-image id="btn01" id-video="defensoreshidricos_persona01" src="#defensoreshidricos_btn1" id-src="defensoreshidricos_btn1" width="1.18" height="1" geometry="width: 7.66; height: 1.1" position="0 -0.74 0.2" btn-click="Video" >
                    <a-image id="defensoreshidricos_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.77 0.6 0.1"></a-image>
                </a-image>
            </a-entity>`,

            `<a-entity position="4 5 13 " rotation="0 180 0">
                <a-image src="#healthcenter_liga" id-src="healthcenter_liga" width="1.18" height="1" geometry="width: 5.21; height: 3.12" btn-click="Liga" liga="comunidades/healthcenter" >
                    <a-image id="comunidades/healthcenter_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.4 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="-4.5 7.5 -10" rotation="0 25 0">
                <a-image src="#parque_liga" id-src="parque_liga" width="1.18" height="1" geometry="width: 4.82; height: 3.12" btn-click="Liga" liga="comunidades/parque" >
                    <a-image id="comunidades/parque_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.2 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="5.5 8 -12" rotation="0 340 0">
                <a-image src="#finca_liga" id-src="finca_liga" width="1.18" height="1" geometry="width: 5.17; height: 3.12" btn-click="Liga" liga="comunidades/finca" >
                    <a-image id="comunidades/finca_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.4 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,

            `<a-videosphere id="persona" src="#defensoreshidricos_persona01" geometry="radius: 99; phiLength: 28.67; thetaLength: 61.28; thetaStart: 72.76; phiStart: 217.8" material="color: #fcfcfc"></a-videosphere>`,
            `<a-sky radius="100" id="sky" src="#defensoreshidricos_bg" load-obj></a-sky>`
        ]
    },
    "finca":{
        "name":"Explora una finca de productos DONLAP",
        "positionCamera":1.6,
        "rotateCamera":120,
        "ambiente":false,
        "voz":true,
        "onlyView":true,
        "assets":[
            '<img id="finca_bg" src="assets/islas/comunidades/finca/images/bg.jpg">',
            '<video  id="finca_loop01" loop="true" src="assets/islas/comunidades/finca/videos/loop01.mp4" preload="auto" class="loopVideo" crossorigin="anonymous" webkit-playsinline playsinline></video>'
        ],
        "elements":[
            '<a-videosphere id="loop01_V" src="#finca_loop01" geometry="radius: 99; phiLength: 26.85; thetaLength: 26.71; thetaStart: 79.43; phiStart: 121" material="color: #ffffff"></a-videosphere>',
            '<a-sky radius="100" src="#finca_bg" load-obj="Sky"></a-sky>'
        ]
    },
     "healthcenter":{
        "name":"Fortaleciendo la salud de las comunidades",
        "positionCamera":0,
        "rotateCamera":110,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<!-- FONDO -->',
            '<img id="healthcenter_bg" src="assets/islas/comunidades/healthcenter/images/bg.jpeg">',
            '<!-- PERSONA(S) -->',
            '<video  id="healthcenter_persona01" loop="false" src="assets/islas/comunidades/healthcenter/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="healthcenter_persona02" loop="false" src="assets/islas/comunidades/healthcenter/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<!-- OPCION MULTIPLE -->',
            '<img id="healthcenter_btn1" src="assets/islas/comunidades/healthcenter/images/btn1.png">',
            '<img id="healthcenter_btn1_press" src="assets/islas/comunidades/healthcenter/images/btn1_press.png">',
            '<img id="healthcenter_btn2" src="assets/islas/comunidades/healthcenter/images/btn2.png">',
            '<img id="healthcenter_btn2_press" src="assets/islas/comunidades/healthcenter/images/btn2_press.png">',
            '<img id="healthcenter_pleca" src="assets/islas/comunidades/healthcenter/images/pleca.png"></img>',
        ],
        "elements":[
            `<a-entity id="opciones" position="-9 -2.5 9" scale="1 1 1" rotation="0 105 0" load-obj="opciones">
            <a-image id="pleca" src="#healthcenter_pleca" width="1.18" height="1" geometry="width: 9.98; height: 2.08" material=""></a-image>
                <a-image id="btn01" id-video="healthcenter_persona01" src="#healthcenter_btn1" id-src="healthcenter_btn1" width="1.18" height="1" geometry="width: 5.65; height: 1.1" position="-4 -1 0.2" btn-click="Video" material="" class="">
                    <a-image id="healthcenter_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.75 0.6 0.1"></a-image>
                </a-image>
                <a-image id="btn02" id-video="healthcenter_persona02" src="#healthcenter_btn2" id-src="healthcenter_btn2" width="1.18" height="1" geometry="width: 7.98; height: 1.1" position="3 -1 0.2" btn-click="Video" material="" class="">
                    <a-image id="healthcenter_persona02_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-4 0.6 0.1"></a-image>
                </a-image>
            </a-entity>`,
            
            `<a-entity position="12 5.6 0" rotation="0 275 0">
                <a-image src="#defensoreshidricos_liga" id-src="defensoreshidricos_liga" width="1.18" height="1" geometry="width: 5.21; height: 3.12" btn-click="Liga" liga="comunidades/defensoreshidricos" material="" class="">
                    <a-image id="comunidades/defensoreshidricos_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.5 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="-7 5 11" rotation="0 130 0">
                <a-image src="#iptcoclecito_liga" id-src="iptcoclecito_liga" width="1.18" height="1" geometry="width: 5.21; height: 3.12" btn-click="Liga" liga="comunidades/iptcoclecito" material="" class="">
                    <a-image id="comunidades/iptcoclecito_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.5 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="0.4 4.5 -12.4" rotation="0 10 0">
                <a-image src="#testimonio_liga" id-src="testimonio_liga" width="1.18" height="1" geometry="width: 5.21; height: 3.12" btn-click="Liga" liga="comunidades/testimonio" material="" class="">
                    <a-image id="comunidades/testimonio_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.5 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-videosphere id="persona" src="#healthcenter_persona01" geometry="radius: 99; phiLength: 46.7; thetaLength: 70.2; thetaStart: 80.95; phiStart: 160.33" visible="" load-obj="" material=""></a-videosphere>`,
            `<a-sky radius="100" src="#healthcenter_bg" color="" load-obj></a-sky>`,
        ]
    },
    "historia":{
        "name":"Conoce la historia de Café La Ceiba",
        "positionCamera":1.6,
        "rotateCamera":-30,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="historia_bg" src="assets/islas/comunidades/historia/images/bg.jpeg">',
            
            '<video  id="historia_persona01" loop="false" src="assets/islas/comunidades/historia/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="historia_persona02" loop="false" src="assets/islas/comunidades/historia/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="historia_persona03" loop="false" src="assets/islas/comunidades/historia/videos/persona03.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            
            '<img id="historia_btn01" src="assets/islas/comunidades/historia/images/btn1.png">',
            '<img id="historia_btn01_press" src="assets/islas/comunidades/historia/images/btn1_press.png">',
            '<img id="historia_btn02" src="assets/islas/comunidades/historia/images/btn2.png">',
            '<img id="historia_btn02_press" src="assets/islas/comunidades/historia/images/btn2_press.png">',
            '<img id="historia_btn03" src="assets/islas/comunidades/historia/images/btn3.png">',
            '<img id="historia_btn03_press" src="assets/islas/comunidades/historia/images/btn3_press.png">',
            '<img id="historia_pleca" src="assets/islas/comunidades/historia/images/pleca.png">'
        ],
        "elements":[
            `<a-entity id="opciones" position="3.5 1 -12" scale="1 1 1" rotation="0 -25 0" load-obj="opciones">
                <a-image id="pleca" src="#historia_pleca" geometry="width: 9.98; height: 1.5"></a-image>
                <a-image id="btn01" id-video="historia_persona01" src="#historia_btn01" id-src="historia_btn01" geometry="width: 6; height: 1.1" position="-6 -0.7 0.2" btn-click="Video" material="">
                    <a-image id="historia_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.8 0.6 0.1"></a-image>
                </a-image>
                <a-image id="btn02" id-video="historia_persona02" src="#historia_btn02" id-src="historia_btn02" geometry="width: 6; height: 1.1" position="0.15 -0.7 0.2" btn-click="Video" >
                    <a-image id="historia_persona02_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.8 0.6 0.1"></a-image>
                </a-image>
                <a-image id="btn03" id-video="historia_persona03" src="#historia_btn03" id-src="historia_btn03" geometry="width: 5; height: 1.1" position="5.8 -0.7 0.2" btn-click="Video" >
                    <a-image id="historia_persona03_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.4 0.6 0.1"></a-image>
                </a-image>
            </a-entity>`,

            `<a-entity position="-.5 5 9" rotation="0 170 0">
                <a-image src="#testimonio_liga" id-src="testimonio_liga" geometry="width: 5.21; height: 3.12" btn-click="Liga" liga="comunidades/testimonio">
                    <a-image id="comunidades/testimonio_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.4 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="12 6.5.6" rotation="0 285 0">
                <a-image src="#ceibafarm_liga" id-src="ceibafarm_liga" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="comunidades/ceibafarm">
                    <a-image id="comunidades/ceibafarm_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.85 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-videosphere id="persona" src="#historia_persona01" geometry="radius: 99; phiLength: 32.25; thetaLength: 64.97; thetaStart: 64.8; phiStart: 313.32" visible="" load-obj="" material=""></a-videosphere>`,
            `<a-sky radius="100" src="#historia_bg" load-obj></a-sky>`
        ]
    },
    "iptcoclecito":{
        "name":"Conoce los programas educativos de la mina",
        "positionCamera":1.6,
        "rotateCamera":40,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="iptcoclecito_bg" src="assets/islas/comunidades/iptcoclecito/images/bg.jpeg">',
            '<video  id="iptcoclecito_persona01" loop="false" src="assets/islas/comunidades/iptcoclecito/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="iptcoclecito_persona02" loop="false" src="assets/islas/comunidades/iptcoclecito/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<img id="iptcoclecito_btn1" src="assets/islas/comunidades/iptcoclecito/images/btn1.png">',
            '<img id="iptcoclecito_btn1_press" src="assets/islas/comunidades/iptcoclecito/images/btn1_press.png">',
            '<img id="iptcoclecito_btn2" src="assets/islas/comunidades/iptcoclecito/images/btn2.png">',
            '<img id="iptcoclecito_btn2_press" src="assets/islas/comunidades/iptcoclecito/images/btn2_press.png">',
            '<img id="iptcoclecito_pleca" src="assets/islas/comunidades/iptcoclecito/images/pleca.png">',
        ],
        "elements":[
            `<a-entity id="opciones" position="-5.5 .5 -10" scale="1 1 1" rotation="-10 30 0" load-obj="opciones">
                <a-image src="#iptcoclecito_pleca" width="1.18" height="1" geometry="width: 11.020; height: 1.550"></a-image>
                <a-image id="btn01" id-video="iptcoclecito_persona01" src="#iptcoclecito_btn1" id-src="iptcoclecito_btn1" width="1.18" height="1" geometry="width: 5.65; height: 1.1" position="-3.2 -0.7 0.2" btn-click="Video" material="">
                    <a-image src="#palomita" id="iptcoclecito_persona01" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.8 0.6 0.1"></a-image>
                </a-image>
                <a-image id="btn02" id-video="iptcoclecito_persona02" src="#iptcoclecito_btn2" id-src="iptcoclecito_btn2" width="1.18" height="1" geometry="width: 6.32; height: 1.1" position="3 -0.7 0.2" btn-click="Video" >
                    <a-image src="#palomita" id="iptcoclecito_persona02" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.1 0.6 0.1"></a-image>
                </a-image>
            </a-entity>`,

            `<a-entity position="-10.5 8 7.6" rotation="0 120 0">
                <a-image src="#caminos_liga" id-src="caminos_liga" width="1.18" height="1" geometry="width: 4.82; height: 3.12" btn-click="Liga" liga="comunidades/caminos">
                    <a-image id="comunidades/caminos_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.25 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="-2.8 9 -10" rotation="0 20 0">
                <a-image src="#historia_liga" id-src="historia_liga" width="1.18" height="1" geometry="width: 5.21; height: 3.12" btn-click="Liga" liga="comunidades/historia">
                    <a-image id="comunidades/historia_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.4 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="13 7 4" rotation="0 260 0">
                <a-image src="#parque_liga" id-src="parque_liga" width="1.18" height="1" geometry="width: 4.82; height: 3.12" btn-click="Liga" liga="comunidades/parque">
                    <a-image id="comunidades/parque_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.25 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-videosphere id="persona" src="#iptcoclecito_persona02" geometry="radius: 99; phiLength: 40.4; thetaLength: 81.6; thetaStart: 60.7; phiStart: 179.6" visible="" load-obj="" material=""></a-videosphere>`,
            `<a-sky radius="100" src="#iptcoclecito_bg" color="" load-obj></a-sky>`
        ]
    },
    "parque":{
        "name":"Conoce nuestra filosofía social",
        "positionCamera":1.6,
        "rotateCamera":55,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="parque_bg" src="assets/islas/comunidades/parque/images/bg.jpeg">',

            '<video  id="parque_persona01" loop="false" src="assets/islas/comunidades/parque/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="parque_persona02" loop="false" src="assets/islas/comunidades/parque/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="parque_persona03" loop="false" src="assets/islas/comunidades/parque/videos/persona03.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            
            '<img id="parque_btn01" src="assets/islas/comunidades/parque/images/btn1.png">',
            '<img id="parque_btn01_press" src="assets/islas/comunidades/parque/images/btn1_press.png">',
            '<img id="parque_btn02" src="assets/islas/comunidades/parque/images/btn2.png">',
            '<img id="parque_btn02_press" src="assets/islas/comunidades/parque/images/btn2_press.png">',
            '<img id="parque_btn03" src="assets/islas/comunidades/parque/images/btn3.png">',
            '<img id="parque_btn03_press" src="assets/islas/comunidades/parque/images/btn3_press.png">',
            '<img id="parque_pleca" src="assets/islas/comunidades/parque/images/pleca.png">'
        ],
        "elements":[
            `<a-entity id="opciones" position="-7 1.7 -8.2" scale="1 1 1" rotation="0 40 0" load-obj="opciones">
                <a-image id="pleca" src="#parque_pleca" width="1.18" height="1" geometry="width: 8.03; height: 1.93"></a-image>
                <a-image id="btn01" id-video="parque_persona01" src="#parque_btn01" id-src="parque_btn01" width="1.18" height="1" geometry="width: 4.32; height: 1.1" position="-5.37 -1 0.2" btn-click="Video" material="">
                    <a-image id="parque_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.1 0.6 0.1"></a-image>
                </a-image>
                <a-image id="btn02" id-video="parque_persona02" src="#parque_btn02" id-src="parque_btn02" width="1.18" height="1" geometry="width: 5.51; height: 1.1" position="-0.3 -1 0.2" btn-click="Video" >
                    <a-image id="parque_persona02_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.7 0.6 0.1"></a-image>
                </a-image>
                <a-image id="btn03" id-video="parque_persona03" src="#parque_btn03" id-src="parque_btn03" width="1.18" height="1" geometry="width: 6.86; height: 1.1" position="6.04 -1 0.2" btn-click="Video" >
                    <a-image id="parque_persona03_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.4 0.6 0.1"></a-image>
                </a-image>
            </a-entity>`,

            `<a-entity position="-7.5 6 10" rotation="0 130 0">
                <a-image src="#iptcoclecito_liga" id-src="iptcoclecito_liga" width="1.18" height="1" geometry="width: 5.21; height: 3.12" btn-click="Liga" liga="comunidades/iptcoclecito">
                    <a-image id="comunidades/iptcoclecito_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.4 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="1.3 7 -12" rotation="0 0 0">
                <a-image src="#finca_liga" id-src="finca_liga" width="1.18" height="1" geometry="width: 5.17; height: 3.12" btn-click="Liga" liga="comunidades/finca">
                    <a-image id="comunidades/finca_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.4 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-videosphere id="persona" src="#parque_persona01" geometry="radius: 99; phiLength: 69.65; thetaLength: 112.66; thetaStart: 50.79; phiStart: 144.42" visible="" load-obj="" material=""></a-videosphere>`,
            `<a-sky radius="100" src="#parque_bg" load-obj></a-sky>`
        ]
    },
    "testimonio":{
        "name":"Conoce el testimonio de la comunidad",
        "positionCamera":1.6,
        "rotateCamera":40,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="testimonio_bg" src="assets/islas/comunidades/testimonio/images/bg.jpg">',
            '<video id="testimonio_persona01" loop="false" src="assets/islas/comunidades/testimonio/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video id="testimonio_persona02" loop="false" src="assets/islas/comunidades/testimonio/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<img id="testimonio_btn1" src="assets/islas/comunidades/testimonio/images/btn1.png">',
            '<img id="testimonio_btn1_press" src="assets/islas/comunidades/testimonio/images/btn1_press.png">',
            '<img id="testimonio_btn2" src="assets/islas/comunidades/testimonio/images/btn2.png">',
            '<img id="testimonio_btn2_press" src="assets/islas/comunidades/testimonio/images/btn2_press.png">',
            '<img id="testimonio_pleca" src="assets/islas/comunidades/testimonio/images/pleca.png">'
        ],
        "elements":[
            `<a-entity id="opciones" position="-5.7 1.5 -11" scale="1 1 1" rotation="0 35 0" load-obj="opciones">
                <a-image id="pleca" src="#testimonio_pleca" geometry="width: 11.1; height: 1.49"></a-image>
                <a-image id="btn01" id-video="testimonio_persona01" src="#testimonio_btn1" id-src="testimonio_btn1" geometry="width: 5.65; height: 1.1" position="-2.6 -0.7 0.2" btn-click="Video">
                    <a-image id="testimonio_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.7 0.6 0.4"></a-image>
                </a-image>
                <a-image id="btn02" id-video="testimonio_persona02" src="#testimonio_btn2" id-src="testimonio_btn2" geometry="width: 5; height: 1.1" position="2.9 -0.7 0.2" btn-click="Video">
                    <a-image id="testimonio_persona02_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.5 0.6 0.4"></a-image>
                </a-image>
            </a-entity>`,

            `<a-entity position="-10.5 8 7.6" rotation="0 120 0">
                <a-image src="#healthcenter_liga" id-src="healthcenter_liga" width="1.18" height="1" geometry="width: 5.21; height: 3.12" btn-click="Liga" liga="comunidades/healthcenter">
                    <a-image id="comunidades/healthcenter_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.4 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="-2.8 9 -10" rotation="0 20 0">
                <a-image src="#iptcoclecito_liga" id-src="iptcoclecito_liga" width="1.18" height="1" geometry="width: 5.21; height: 3.12" btn-click="Liga" liga="comunidades/iptcoclecito">
                    <a-image id="comunidades/iptcoclecito_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.4 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="13 7 4" rotation="0 260 0">
                <a-image src="#historia_liga" id-src="historia_liga" width="1.18" height="1" geometry="width: 5.21; height: 3.12" btn-click="Liga" liga="comunidades/historia">
                    <a-image id="comunidades/historia_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.4 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-videosphere id="persona" src="#testimonio_persona01" geometry="radius: 99; phiLength: 26.99; thetaLength: 52.84; thetaStart: 73.29; phiStart: 165.62" material=""></a-videosphere>`,
            `<a-sky radius="100" src="#testimonio_bg" load-obj></a-sky>`
        ]
    }
}