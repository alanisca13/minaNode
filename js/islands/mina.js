const minaArray = {
    "camion":{
        "name":"Explora el camión más grande del mundo",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":-90,
        "ambiente":false,
        "voz":true,
        "onlyView":true,
        "assets":[
            `<video  id="camion_loop01" loop="true" src="assets/islas/mina/camion/videos/loop01.mp4" preload="auto" class="loopVideo" crossorigin="anonymous" webkit-playsinline playsinline></video>`
        ],
        "elements":[
            `<a-videosphere id="loop01_V" src="#camion_loop01" geometry="radius: 99;"></a-videosphere>`
        ]
    },
    "gigantes":{
        "name":"Explora cómo trabajan los gigantes",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":70,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="gigantes_bg" src="assets/islas/mina/gigantes/images/bg.jpg">',
            '<video  id="gigantes_persona01" loop="false" src="assets/islas/mina/gigantes/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>',
            '<video  id="gigantes_persona02" loop="false" src="assets/islas/mina/gigantes/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>',
            '<video id="gigantes_loop01" loop="true" src="assets/islas/mina/gigantes/videos/loop01.mp4" class="loopVideo" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',

            '<img id="gigantes_btn1" src="assets/islas/mina/gigantes/images/btn1.png">',
            '<img id="gigantes_btn1_press" src="assets/islas/mina/gigantes/images/btn1_press.png">',
            '<img id="gigantes_btn2" src="assets/islas/mina/gigantes/images/btn2.png">',
            '<img id="gigantes_btn2_press" src="assets/islas/mina/gigantes/images/btn2_press.png">',
            '<img id="gigantes_tema" src="assets/islas/mina/gigantes/images/tema.png">'
        ],
        "elements":[
            
            `<a-entity id="opciones" position="-7 -1 6" scale="1 1 1" rotation="0 120 0" load-obj="opciones">
                <a-image id="pleca" src="#gigantes_tema" width="1.18" height="1" geometry="width: 8.02; height: 1.55"></a-image>
                <a-image id="btn01" id-video="gigantes_persona01" src="#gigantes_btn1" id-src="gigantes_btn1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="-3.3 -.7 0.34" btn-click="Video">
                    <a-image id="gigantes_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 0.5 0.1"></a-image>
                </a-image>
                <a-image id="btn02" id-video="gigantes_persona02" src="#gigantes_btn2" id-src="gigantes_btn2" width="1.18" height="1" geometry="width: 5.71; height: 1.1" position="1.8 -.7 0.31" btn-click="Video">
                    <a-image id="gigantes_persona02_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.8 0.5 0.1"></a-image>
                </a-image>
            </a-entity>`,
            '<a-videosphere id="persona" src="#gigantes_persona01" geometry="radius: 98; phiLength: 26.79; thetaLength: 54.82; thetaStart: 78.19; phiStart: 165.92" material="color: #ffffff"></a-videosphere>',
            '<a-videosphere id="loop01_V" src="#gigantes_loop01" geometry="radius: 99; phiLength: 266.89; thetaLength: 45.37; thetaStart: 57.67; phiStart: 79.12" material="" visible=""></a-videosphere>',

            `<a-entity position="0 5 10" rotation="0 180 0">
                <a-image src="#camion_liga" id-src="camion_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="mina/camion">
                    <a-image id="mina/camion_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="2.8 7 -13" rotation="0 340 0">
                <a-image src="#simulacion_liga" id-src="simulacion_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="mina/simulacion">
                    <a-image id="mina/simulacion_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
                </a-image>
            </a-entity>`,
            
            `<a-sky radius="100" src="#gigantes_bg" load-obj></a-sky>`
        ]
    },
    "mina":{
        "name":"Visita la mina",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":120,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="mina_bg" src="assets/islas/mina/mina/images/bg.jpg">',

            '<video  id="mina_persona01" loop="false" src="assets/islas/mina/mina/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="mina_persona02" loop="false" src="assets/islas/mina/mina/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',

            '<img id="mina_btn1" src="assets/islas/mina/mina/images/btn1.png">',
            '<img id="mina_btn1_press" src="assets/islas/mina/mina/images/btn1_press.png">',
            '<img id="mina_btn2" src="assets/islas/mina/mina/images/btn2.png">',
            '<img id="mina_btn2_press" src="assets/islas/mina/mina/images/btn2_press.png">',
            '<img id="mina_pleca" src="assets/islas/mina/mina/images/tema.png">'
        ],
        "elements":[
            `<a-entity id="opciones" position="-6.5 1.6 7" scale="1 1 1" rotation="0 130 0" load-obj="opciones">
                <a-image id="pleca" src="#mina_pleca" width="1.18" height="1" geometry="width: 8; height: 1.65" material=""></a-image>
                <a-image id="btn01" id-video="mina_persona01" src="#mina_btn1" id-src="mina_btn1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="-2.5 -0.7 0.2" btn-click="Video" material="" class="">
                    <a-image id="mina_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 0.5 0.1"></a-image>
                </a-image>
                <a-image id="btn02" id-video="mina_persona02" src="#mina_btn2" id-src="mina_btn2" width="1.18" height="1" geometry="width: 4.62; height: 1.1" position="2.5 -0.7 0.2" btn-click="Video" material="" class="">
                    <a-image id="mina_persona02_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.2 0.6 0.4"></a-image>
                </a-image>
            </a-entity>`,
            `<a-videosphere id="persona" src="#mina_persona01" geometry="radius: 98; phiLength: 36.74; thetaLength: 71.86; thetaStart: 58.52; phiStart: 166.87" visible="" load-obj="" material=""></a-videosphere>`,
            `<a-entity position="0.4 4.5 -12.4" rotation="0 -10 0">
                <a-image src="#gigantes_liga" id-src="gigantes_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="mina/gigantes" material="">
                    <a-image id="mina/gigantes_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="-3.2 5 -8.5" rotation="0 20 0">
                <a-image src="#palaelectrica_liga" id-src="palaelectrica_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="mina/palaelectrica" class="">
                    <a-image id="mina/palaelectrica_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="-9.6 7.2 -8.9" rotation="0 50 0">
                <a-image src="#vuela_liga" id-src="vuela_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="mina/vuela">
                    <a-image id="mina/vuela_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
                </a-image>
            </a-entity>`,
            `<a-sky radius="100" src="#mina_bg" color="" load-obj></a-sky>`
        ]
    },
    "palaelectrica":{
        "name":"Explora la pala eléctrica más grande del mundo",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":20,
        "ambiente":true,
        "voz":true,
        "onlyView":true,
        "assets":[
            '<video  id="palaelectrica_loop01" loop="true" src="assets/islas/mina/palaelectrica/videos/loop01.mp4" class="loopVideo" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>'
        ],
        "elements":[
            '<a-videosphere id="loop01_V" src="#palaelectrica_loop01" geometry="radius: 99;"></a-videosphere>'
        ]
    },
    "simulacion":{
        "name":"Visita la sala de simulación",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":100,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="simulacion_bg" src="assets/islas/mina/simulacion/images/bg.jpeg">',
            '<video id="simulacion_persona01" loop="false" src="assets/islas/mina/simulacion/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video id="simulacion_persona02" loop="false" src="assets/islas/mina/simulacion/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video id="simulacion_loop01" loop="true" src="assets/islas/mina/simulacion/videos/loop01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<img id="simulacion_btn1" src="assets/islas/mina/simulacion/images/btn1.png">',
            '<img id="simulacion_btn1_press" src="assets/islas/mina/simulacion/images/btn1_press.png">',
            '<img id="simulacion_btn2" src="assets/islas/mina/simulacion/images/btn2.png">',
            '<img id="simulacion_btn2_press" src="assets/islas/mina/simulacion/images/btn2_press.png">',
            '<img id="simulacion_tema" src="assets/islas/mina/simulacion/images/pleca.png">'
        ],

        "elements":[
            `<a-entity id="opciones" position="-4.549 1 -2.7" scale="0.6 0.6 0.6" rotation="0 70 0" load-obj="">
                <a-image id="pleca" src="#simulacion_tema" width="1.18" height="1" geometry="width: 9.02; height: 1.93"></a-image>
                <a-image id="btn01" src="#simulacion_btn1" id-src="simulacion_btn1" width="1.18" height="1" geometry="width: 6.65; height: 1.1" position="3.4 -.7 0.2" btn-click="Video" id-video="simulacion_persona01" material="" text__molienda="">
                    <a-image id="simulacion_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.3 .5 0.1"></a-image>
                </a-image>
                <a-image id="btn02" src="#simulacion_btn2" id-src="simulacion_btn2" width="1.18" height="1" geometry="width: 5.71; height: 1.1" position="-2.9 -.7 0.2" btn-click="Video" id-video="simulacion_persona02" material="" text__molienda="">
                    <a-image id="simulacion_persona02_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.8 .5 .1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-videosphere id="persona" src="#simulacion_persona01" geometry="radius: 98; phiLength: 41.6; thetaLength: 84.5; thetaStart: 66.8; phiStart: 129" material="color: #ffffff"></a-videosphere>`,
            `<a-videosphere id="loop01_V" src="#simulacion_loop01" geometry="radius: 99; phiLength: 75.13; thetaLength: 65.18; thetaStart: 70.39; phiStart: 172.03" material="" visible=""></a-videosphere>`,
            `<a-entity position="-9 5 -6" rotation="0 75 0">
                <a-image src="#liga" id-src="liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="mina/mina">
                    <a-image id="mina/mina_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
                </a-image>
            </a-entity>`,
            `<a-sky radius="100" src="#simulacion_bg" color="" load-obj></a-sky>`
        ]
    },
    "vuela":{
        "name":"Vuela sobre la mina",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":0,
        "ambiente":true,
        "voz":false,
        "onlyView":true,
        "assets":[
            '<video  id="vuela_loop01" loop="true" src="assets/islas/mina/vuela/videos/loop01.mp4" class="loopVideo" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>'
        ],
        "elements":[
            '<a-videosphere id="loop01_V" src="#vuela_loop01" geometry="radius: 99;"></a-videosphere>'
        ]
    }
}