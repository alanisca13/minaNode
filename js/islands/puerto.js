const puertoArray = {
    "almacenamiento":{
        "name":"Visita el almacenamiento de Concentrado de Cobre",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":40,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="almacenamiento_bg" src="assets/islas/puerto/almacenamiento/images/bg.jpeg">',
            '<video  id="almacenamiento_persona01" loop="false" src="assets/islas/puerto/almacenamiento/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="almacenamiento_persona02" loop="false" src="assets/islas/puerto/almacenamiento/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="almacenamiento_persona03" loop="false" src="assets/islas/puerto/almacenamiento/videos/persona03.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="almacenamiento_persona04" loop="false" src="assets/islas/puerto/almacenamiento/videos/persona04.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video  id="almacenamiento_loop01" loop="true" src="assets/islas/puerto/almacenamiento/videos/loop01.mp4" preload="auto" class="loopVideo" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<img id="almacenamiento_mina_btn1" src="assets/islas/puerto/almacenamiento/images/btn1.png">',
            '<img id="almacenamiento_mina_btn1_press" src="assets/islas/puerto/almacenamiento/images/btn1_press.png">',
            '<img id="almacenamiento_mina_btn2" src="assets/islas/puerto/almacenamiento/images/btn2.png">',
            '<img id="almacenamiento_mina_btn2_press" src="assets/islas/puerto/almacenamiento/images/btn2_press.png">',
            '<img id="almacenamiento_mina_btn3" src="assets/islas/puerto/almacenamiento/images/btn3.png">',
            '<img id="almacenamiento_mina_btn3_press" src="assets/islas/puerto/almacenamiento/images/btn3_press.png">',
            '<img id="almacenamiento_mina_btn4" src="assets/islas/puerto/almacenamiento/images/btn4.png">',
            '<img id="almacenamiento_mina_btn4_press" src="assets/islas/puerto/almacenamiento/images/btn4_press.png">',
            '<img id="almacenamiento_mina_pleca" src="assets/islas/puerto/almacenamiento/images/pleca.png">'
        ],
        "elements":[
            `<a-entity id="opciones" position="-6 0 -14" scale="1 1 1" rotation="-15 40 0" load-obj="opciones">
                <a-image id="pleca" src="#almacenamiento_mina_pleca" width="1.18" height="1" geometry="width: 10.46; height: 1.66"></a-image>
                <a-image id="btn01" id-video="almacenamiento_persona01" src="#almacenamiento_mina_btn1" id-src="almacenamiento_mina_btn1" width="1.18" height="1" geometry="width: 4.79; height: 1.1" position="-3.8 -0.7 0.2" class="collidable" btn-click="Video" material="">
                    <a-image id="almacenamiento_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.3 0.5 0.4"></a-image>
                </a-image>
                <a-image id="btn02" id-video="almacenamiento_persona02" src="#almacenamiento_mina_btn2" id-src="almacenamiento_mina_btn2" width="1.18" height="1" geometry="width: 7.43; height: 1.1" position="2.5 -0.7 0.2" class="collidable" btn-click="Video" material="">
                    <a-image id="almacenamiento_persona02_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.7 0.5 0.4"></a-image>
                </a-image>
                <a-image id="btn03" id-video="almacenamiento_persona03" src="#almacenamiento_mina_btn3" id-src="almacenamiento_mina_btn3" width="1.18" height="1" geometry="width: 6.29; height: 1.1" position="-3 -1.9 0.2" class="collidable" btn-click="Video" material="">
                    <a-image id="almacenamiento_persona03_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.1 0.6 0.4"></a-image>
                </a-image>
                <a-image id="btn04" id-video="almacenamiento_persona04" src="#almacenamiento_mina_btn4" id-src="almacenamiento_mina_btn4" width="1.18" height="1" geometry="width: 6.63; height: 1.1" position="3.55 -1.9 0.2" class="collidable" btn-click="Video" material="">
                    <a-image id="almacenamiento_persona04_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.4 0.6 0.4"></a-image>
                </a-image>
            </a-entity>`,
            `<a-videosphere id="persona" src="#almacenamiento_persona01" geometry="radius: 98; phiLength: 27.48; thetaLength: 66.8; thetaStart: 75.3; phiStart: 185.45" material=""></a-videosphere>`,
            `<a-videosphere id="loop01_V" src="#almacenamiento_loop01" geometry="radius: 99; phiLength: 203.52; thetaLength: 107.87" material=""></a-videosphere>`,
            
            `<a-entity position="8.1 6.9 1.3" rotation="0 270 0">
                <a-image src="#caribe_liga" id-src="caribe_liga" width="1.18" height="1" geometry="width: 4.93; height: 3.06" material="" class="collidable" btn-click="Liga" liga="puerto/caribe">
                    <a-image id="puerto/caribe_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.2 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="8 5.3 -10" rotation="0 315 0">
                <a-image src="#plantaenergia_liga" id-src="plantaenergia_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" class="collidable" btn-click="Liga" liga="puerto/plantaenergia">
                    <a-image id="puerto/plantaenergia_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="1.1 5 -12.5" rotation="0 -10.3 0">
                <a-image src="#plumacarga_liga" id-src="plumacarga_liga" width="1.18" height="1" geometry="width: 5.25; height: 3.05" material="" class="collidable" btn-click="Liga" liga="puerto/plumacarga">
                    <a-image id="puerto/plumacarga_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.4 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="-15 6.5 2.77" rotation="0 89.45 0">
                <a-image src="#galera_liga" id-src="galera_liga" width="1.18" height="1" geometry="width: 4.82; height: 3.12" material="" class="collidable" btn-click="Liga" liga="puerto/galera">
                    <a-image id="puerto/galera_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.2 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,

            `<a-sky radius="100" src="#almacenamiento_bg" load-obj></a-sky>`
        ]
    },
    "caribe":{
        "name":"Visita la Planta de Energía",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":0,
        "ambiente":true,
        "voz":true,
        "onlyView":true,
        "assets":[
            `<video  id="caribe_loop01" loop="true" src="assets/islas/puerto/caribe/videos/bg.mp4" class="loopVideo" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>`
        ],
        "elements":[
            '<a-videosphere id="loop01_V" src="#caribe_loop01" geometry="radius: 99;"></a-videosphere>'
        ]
    },
    "galera":{
        "name":"Explora el recuperador mecánico",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":140,
        "ambiente":true,
        "voz":true,
        "onlyView":true,
        "assets":[
            '<video  id="galera_loop01" loop="true" src="assets/islas/puerto/galera/videos/bg.mp4" class="loopVideo" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>'
        ],
        "elements":[
            '<a-videosphere id="loop01_V" src="#galera_loop01" geometry="radius: 99;"></a-videosphere>'
        ]
    },
    "plantaenergia":{
        "name":"Visita la Planta de Energía",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":110,
        "ambiente":true,
        "voz":false,
        "onlyView":false,
        "assets":[
            '<img id="plantaenergia_bg" src="assets/islas/puerto/plantaenergia/images/bg.jpg">',
            '<video id="plantaenergia_persona01" loop="false" src="assets/islas/puerto/plantaenergia/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<video id="plantaenergia_persona02" loop="false" src="assets/islas/puerto/plantaenergia/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>',
            '<img id="plantaenergia_btn1" src="assets/islas/puerto/plantaenergia/images/btn1.png">',
            '<img id="plantaenergia_btn1_press" src="assets/islas/puerto/plantaenergia/images/btn1_press.png">',
            '<img id="plantaenergia_btn2" src="assets/islas/puerto/plantaenergia/images/btn2.png">',
            '<img id="plantaenergia_btn2_press" src="assets/islas/puerto/plantaenergia/images/btn2_press.png">',
            '<img id="plantaenergia_pleca" src="assets/islas/puerto/plantaenergia/images/pleca.png">',
        ],
        "elements":[
            `<a-entity position="-1 6.5 8" rotation="0 155 0">
                <a-image src="#almacenamiento_liga" id-src="almacenamiento_liga" width="1.18" height="1" geometry="width: 3.9; height: 3.12" class="collidable" btn-click="Liga" liga="puerto/almacenamiento" material="" class="">
                    <a-image id="puerto/almacenamiento_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity position="-12.2 7 -7" rotation="0 90 0">
                <a-image src="#vuelaplantaenergia_liga" id-src="vuelaplantaenergia_liga" width="1.18" height="1" geometry="width: 3.92; height: 3.12"  class="collidable" btn-click="Liga" liga="puerto/vuelaplantaenergia">
                    <a-image id="puerto/vuelaplantaenergia_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.4 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-entity id="opciones" position="-12.5 1.5 3.8" scale="1 1 1" rotation="0 105 0">
                <a-image id="pleca" src="#plantaenergia_pleca" width="1.18" height="1" geometry="width: 10.93; height: 1.66"></a-image>
                <a-image id="btn01" id-video="plantaenergia_persona01" src="#plantaenergia_btn1" id-src="plantaenergia_btn1" width="1.18" height="1" geometry="width: 6.9; height: 1.4" position="-3.5 -0.8 0.2" class="collidable" btn-click="Video">
                    <a-image id="plantaenergia_persona01_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.4 0.6 0.1"></a-image>
                </a-image>
                <a-image id="btn02" id-video="plantaenergia_persona02" src="#plantaenergia_btn2" id-src="plantaenergia_btn2" width="1.18" height="1" geometry="width: 6.9; height: 1.4" position="3.5 -0.8 0.2" class="collidable" btn-click="Video">
                    <a-image id="plantaenergia_persona02_p" src="#palomita" scale="0 0 0" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.4 0.6 0.1"></a-image>
                </a-image>
            </a-entity>`,
            `<a-videosphere id="persona" src="#plantaenergia_persona01" geometry="radius: 99; phiLength: 27.01; thetaLength: 58.16; thetaStart: 64.39; phiStart: 111.23" material="color: #ededed"></a-videosphere>`,
            '<a-sky radius="100" src="#plantaenergia_bg" color="#e7edf3" load-obj></a-sky>'
        ]
    },
    "plumacarga":{
        "name":"",
        "camera":true,
        "positionCamera":-60,
        "rotateCamera":230,
        "ambiente":true,
        "voz":true,
        "onlyView":true,
        "assets":[
            '<video  id="plumacarga_loop01" loop="true" src="assets/islas/puerto/plumacarga/videos/bg.mp4" class="loopVideo" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>'
        ],
        "elements":[
            '<a-videosphere id="loop01_V" src="#plumacarga_loop01" geometry="radius: 99;"></a-videosphere>'
        ]
    },
    "vuelaplantaenergia":{
        "name":"Vuela sobre la Planta de Energía",
        "camera":true,
        "positionCamera":1.6,
        "rotateCamera":140,
        "ambiente":false,
        "voz":true,
        "onlyView":true,
        "assets":[
            '<video  id="vuelaplantaenergia_loop01" loop="true" src="assets/islas/puerto/vuelaplantaenergia/videos/bg.mp4" class="loopVideo" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>'
        ],
        "elements":[
            '<a-videosphere id="loop01_V" src="#vuelaplantaenergia_loop01" geometry="radius: 99;"></a-videosphere>'
        ]
    }
}